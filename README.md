## Hello world!
This project is a documentation of my 22 week journey with Alfatraining for a certified foundation in web development.

## Demo
Visit the [demo](https://agron.dev/) page.

## Getting Started

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

First, clone or fork the project:

```bash
git clone https://gitlab.com/FirstTerraner/alfatraining.git
# change directory
cd alfatraining
# install dependencies
npm i
```

Run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying files.

## Gitlab API usage

To use the GitLab API's, you will need your own PRIVATE_TOKEN. Get [yours](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and create a .env file in the project root directory.
```bash
# file content
PRIVATE_TOKEN=YOUR_PRIVATE_TOKEN
# That's it!
```

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
