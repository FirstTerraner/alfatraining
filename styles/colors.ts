export const themes = {
	dark: {
		background: '#202124',
		color: '#bdc1c6',
		activeMenu: '#34363a',
		border: '#333'
	},
	light: {
		background: '#f8f8f8',
		color: '#444',
		activeMenu: '#f2f2f2',
		border: '#eaeaea'
	},
	muiBlue: '#3f51b5'
}