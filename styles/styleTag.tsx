import { usePrefContext } from '../src/context/preferences'

export default function StyleTag() {

	const { isDark } = usePrefContext()

	// style jsx global section c&p from https://stackoverflow.com/questions/63247550/nextjs-switch-body-background-color
	return (
		<style jsx global>{`
  body {
    background-color: ${isDark ? '#202124' : '#f8f8f8'};
	color: ${isDark ? '#bdc1c6' : '#444'};
  }
`
		}
		</style>
	)
}