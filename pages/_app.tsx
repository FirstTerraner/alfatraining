import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { PrefProvider } from '../src/context/preferences'

export default function MyApp({ Component, pageProps }: AppProps) {
	return (
		<PrefProvider>
			<Component {...pageProps} />
		</PrefProvider>
	)
}