import { GetStaticProps } from 'next'
import Homepage from '../src/components/pages/Home'
import { getAllProjectMembers, getGitlabProject } from '../src/gitlabApis'
import { IHomeProps } from '../src/models'

export const getStaticProps: GetStaticProps = async () => {
	// get project members
	const members = await getAllProjectMembers()
	// get project info
	const gitProject = await getGitlabProject()

	// console.log(gitProject)

	return { props: { members, gitProject }, revalidate: 60 * 60 * 8 }
}

export default function Home(props: IHomeProps) { return <Homepage members={props.members} gitProject={props.gitProject} /> }