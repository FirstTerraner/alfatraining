import React from 'react'
import { getPrefCookie, setPrefCookie } from '../storage/cookies/preferences'
import { IPreferences } from '../models'

const usePreference = () => {

	// TODO refactor preferences states in 1 state
	const [prefs, setPrefs] = React.useState<IPreferences | null>()

	const [cookieAllowed, setCookieAllowed] = React.useState(false)
	const [cookieMsgHidden, setCookieMsgHidden] = React.useState(false)
	const hideCookieMessage = () => {
		setPrefCookie({ dark: false, cookieAllowed: false }, -1)
		setCookieMsgHidden(true)
	}
	const [isDark, setIsDark] = React.useState(false)
	const toggleCookieAllowed = () => {
		if (cookieAllowed) {
			setCookieAllowed(prev => !prev)
			setIsDark(false)
			setPrefCookie({ dark: false, cookieAllowed: false }, -1)
			setPrefs(null)
			return
		}
		setCookieAllowed(prev => !prev)
		setPrefCookie({ dark: isDark, cookieAllowed: !cookieAllowed }, 365)
	}
	const toggleIsDark = () => {
		setIsDark(prev => !prev)
		setPrefCookie({ dark: !isDark, cookieAllowed }, 365)
	}

	React.useEffect(() => {
		/* if (!prefs && !cookieAllowed) {
			return
		} */
		// console.log('prefs: ', prefs)
		// console.log('cookieAllowed: ', cookieAllowed)
		if (!prefs && !cookieAllowed) {
			const preferencesCookie = getPrefCookie()
			// console.log('prefs: ', preferencesCookie)
			if (!preferencesCookie) {
				/* setPrefCookie({ cookieAllowed, dark: false }, 365)
				setPrefs({ cookieAllowed, dark: false }) */
				return
			}
			setPrefs(preferencesCookie)
			setCookieAllowed(preferencesCookie.cookieAllowed)
			setIsDark(preferencesCookie.dark)
			return
		}
		if (prefs) {
			setCookieAllowed(prefs.cookieAllowed)
			setIsDark(prefs.dark)
		}

	}, [prefs, cookieAllowed])

	return {
		cookieAllowed,
		isDark,
		cookieMsgHidden,
		toggleCookie() {
			toggleCookieAllowed()
		},
		toggleDarkMode() {
			toggleIsDark()
		},
		hideCookieMsg() {
			hideCookieMessage()
		}
	}
}
type usePrefType = ReturnType<typeof usePreference>
const PrefContext = React.createContext<usePrefType>({
	cookieAllowed: false,
	isDark: false,
	cookieMsgHidden: false,
	toggleCookie: () => console,
	toggleDarkMode: () => console,
	hideCookieMsg: () => console
})
export const usePrefContext = () => React.useContext(PrefContext)
export const PrefProvider = ({ children }: { children: React.ReactNode }) => (
	<PrefContext.Provider value={usePreference()} >
		{children}
	</PrefContext.Provider>
)