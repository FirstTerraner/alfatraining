import StyleTag from '../../../styles/styleTag'
import CookieBanner from '../CookieBanner'
import Footer from '../Footer'
import Metatags from '../Metatags'
import styles from '../../../styles/Home.module.css'
import Navigation from '../navigation/Navigation'
import { usePrefContext } from '../../context/preferences'
import { Switch } from '@mui/material'

export default function SettingsPage() {

	const { cookieAllowed, toggleCookie } = usePrefContext()

	return (
		<>
			<StyleTag />
			<Metatags
				title='Alfaprojekt Einstellungen'
				description='Speichere deine Layout-Vorlieben'
			/>
			<Navigation />
			<div className={styles.container}>
				<main className={styles.main}>
					<h1>Einstellungen</h1>
					{!cookieAllowed ?
						<p className='centeredTxt'>
							Achtung: Cookies müssen erlaubt werden um Layout-Vorlieben zu speichern!
						</p>
						:
						<p className='centeredTxt'>
							Cookies sind derzeit erlaubt.
						</p>
					}
					<Switch
						aria-label='controlled'
						checked={cookieAllowed}
						onChange={toggleCookie}
					/>
				</main>
			</div>
			<Footer />
			<CookieBanner />
		</>
	)
}