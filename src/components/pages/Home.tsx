import { Avatar, AvatarGroup, Button, Tooltip } from '@mui/material'
import Link from 'next/link'
import styles from '../../../styles/Home.module.css'
import { IHomeProps } from '../../models'
import Footer from '../Footer'
import Metatags from '../Metatags'
import Navigation from '../navigation/Navigation'
import GitProjectInfo from '../GitProjectInfo'
import StyleTag from '../../../styles/styleTag'
import PromoCards from '../promoCards'
import CookieBanner from '../CookieBanner'

export default function Homepage(props: IHomeProps) {

	const { members, gitProject } = props

	return (
		<>
			<StyleTag />
			<Metatags title='Alfaprojekt' description='22 weeks journey in web development with Alfatraining.' />
			<Navigation />
			<div className={styles.container}>
				<main className={styles.main}>
					<h1 className={styles.title}>
						Alfaprojekt
					</h1>
					<h2 className={styles.subTitle}>
						Mein Lehrgang in der Web-Entwicklung mit&nbsp;
						<Link href='https://www.alfatraining.de/gefoerderte-weiterbildung/'>
							<a target='_blank' rel='noopener noreferrer' className={styles.highlightedLink}>Alfatraining</a>
						</Link>
						.
					</h2>
					<h3 className={styles.bigParagraf}>
						Diese Seite dokumentiert einen 22 wöchigen Weiterbildungskurs,
						mit dem Schwerpunkt auf die JavaScript Entwicklung,
						sowohl im Frontend als auch im Backend.
					</h3>
					<Link href='/woche/1/tag/1'>
						<a>
							<Button
								variant='contained'
								color='primary'
								sx={{ margin: '1em 0', width: '250px' }}
							>
								Zum ersten Tag
							</Button>
						</a>
					</Link>
					<div className='flex'>
						<p className={styles.description}>Geschrieben mit Hilfe von:</p>
					</div>
					<div className={styles.grid}>
						<PromoCards
							cards={[
								{
									header: 'React',
									description: 'Finde mehr Informationen über React, dessen Features und Vorteile.',
									link: 'https://reactjs.org/docs/getting-started.html',
									tooltipTitle: 'JavaScript library'
								},
								{
									header: 'Next.js',
									description: 'Lerne mehr über Next.js mit interaktiven Kursen & Quizzes!',
									link: 'https://nextjs.org/learn',
									tooltipTitle: 'React library'
								},
								{
									header: 'Material-UI',
									description: 'Entdecke Material-UI und Ihre React Komponenten-Library.',
									link: 'https://mui.com/',
									tooltipTitle: 'React library'
								},
								{
									header: 'TypeScript',
									description: 'Benutze TypeScript um typische JS Laufzeitfehler zu vermeiden.',
									link: 'https://www.typescriptlang.org/',
									tooltipTitle: 'JavaScript, nur besser'
								},
							]}
						/>
					</div>
					{/* project members */}
					{members && members.length > 0 &&
						<AvatarGroup max={6} sx={{ margin: '1.5em 0' }}>
							{members.map(member => (
								<Tooltip key={member.username} title={member.access_level === 40 ? 'Maintainer' : 'Developer'}>
									<div>
										<Link href={member.web_url}>
											<a target='_blank'>
												<Avatar src={member.avatar_url} sx={{ width: 60, height: 60 }} />
											</a>
										</Link>
									</div>
								</Tooltip>
							))}
						</AvatarGroup>
					}
					<p className='flex'>
						{'< '}
						<Link href='https://gitlab.com/FirstTerraner/alfatraining'>
							<a target='_blank' style={{ textDecoration: 'underline' }}>
								Quellcode
							</a>
						</Link>
						{' />'}
					</p>
					<GitProjectInfo gitProject={gitProject} />
				</main>
			</div>
			<Footer />
			<CookieBanner />
		</>
	)
}