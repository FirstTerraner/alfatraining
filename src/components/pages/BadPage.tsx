import { Button } from '@mui/material'
import Link from 'next/link'
import HomeIcon from '@mui/icons-material/Home'
import styles from '../../../styles/Home.module.css'

export default function BadPage({ noContent }: { noContent?: boolean }) {
	const BORED_EMOJI = <span>&#128530;</span>
	const EMBARRASSED_EMOJI = <span>&#128517;</span>
	return (
		<div className={styles.noContent}>
			{noContent ?
				<h2>
					Noch nichts passiert...&nbsp;{BORED_EMOJI}
				</h2>
				:
				<h2>
					404 | Seite nicht gefunden!&nbsp;{EMBARRASSED_EMOJI}
				</h2>
			}
			<Link href='/'>
				<a>
					<Button variant='outlined' color='primary' endIcon={<HomeIcon />}>
						Home
					</Button>
				</a>
			</Link>
		</div>
	)
}