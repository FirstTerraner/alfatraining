export const codeBlocks = {
	'week6day1': [
		`let stringArray = ['you', 'me', 'we', 'together']

let [val1, val2, val3, val4] = stringArray

let obj = {
    firstname: 'Jessica',
    lastname: 'Jones'
}

let {firstname, lastname} = obj
		`,
		`// Map erzeugen
let myMap = new Map()
myMap.set('firstname', 'Jessica')
myMap.set('lastname', 'Jones')
myMap.set('age', 36)
console.log(myMap.size) // 3

// Map mit Werten erzeugen
let myNewMap = new Map([['firstname', 'Jessica'], ['lastname', 'Jones']])

// Objektdaten in Map umwandeln
let user = {
	firstname: 'Jessica',
	lastname: 'Jones',
	age: 36
}

let userMap = new Map(Object.entries(user))

// Map in Objekt umwandeln
let userObj = Object.fromEntries(userMap)

// Map in Array umwandeln
let userArray = Array.from(myNewMap)

// Map-Schlüssel
let heroMap = new Map()
heroMap.set('firstname', 'Jessica') // String-Schlüssel
heroMap.set(2, 'Number-Schlüssel') 
heroMap.set(2, 'Überschriebener Number-Schlüssel') // Number-Schlüssel überschreiben
heroMap.set('2', 'String-Schlüssel')
heroMap.set(true, 'Boolean-Schlüssel')
heroMap.set(user, 'Objekt-Schlüssel')
heroMap.set({}, 'Leeres Objekt-Schlüssel')
heroMap.set({}, 'Neues Leeres Objekt-Schlüssel') // Überschreibt nicht den vorherigen Objekt Schlüssel

console.log({} === {}) // false für Objekte ohne Referenzen

let emptyObjekt = {} // Referenz
console.log(emptyObject === emptyObject) // true

// Beispiel number-Schlüssel in einem Objekt 
let numberKeyObj = {1: 'Eins'} // {'1': 'Eins'} Number-Schlüssel von Objekten werden in einem String umgewandelt

// Verkürzte Schreibweise
let someMap = new Map()
.set('something', 'new');
// ...

// Value aus der Map auslesen
someMap.get('something') // 'new'
		`,
		`let mySet = new Set()
mySet.add('new value')
console.log(mySet.size) // 1

// Set mit Werten erzeugen
let valueSet = new Set(['one', 'two', 'one'])
console.log(valueSet) // ['one', 'two'] // doppelter Wert wird ignorieren

// Set in einem Array umwandeln mit dem Spread-Operator
let setArray = [...valueSet]

let numberArray = [1,2,2,4,3,6,6]
let uniqueEntriesArray = [...new Set(numberArray)]
		`,
		`
		`

	]
}