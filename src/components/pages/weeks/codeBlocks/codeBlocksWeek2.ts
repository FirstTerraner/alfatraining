export const codeBlocks = {
	'week2day1': [ //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 1
		`
function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

// Prüfen ob der Benutzer Cookies erlaubt
function cookieTester() {
    let canIUseCookies = false
    if (navigator.cookieEnabled) {
        canIUseCookies = true
    }
    if (!canIUseCookies && typeof navigator.cookieEnabled == 'undefined') {
        document.cookie = 'testCookie'
        if (document.cookie.indexOf('testCookie') != -1) {
            canIUseCookies = true
        }
    }
    return canIUseCookies
}

if (cookieTester()) {
    // cookies enabled, set cookie
} else {
    // cookies disabled
}
		`, ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`
// window.sessionStorage
// Speichert nur in aktueller Session / aktuellem Tab

// window.localStorage
// Speichert über aktuelle Session, aktuellen Tab hinaus

// beide Varianten haben gleiche Eingeschaften

// Anzahl der Einträge im Storage ermitteln
console.log(sessionStorage.length)

// Einträge schreiben
sessionStorage.setItem('firstName', 'Jessica')
sessionStorage.setItem('firstName', 'Jessy')
sessionStorage.setItem('lastName', 'Jones')

// Einträge auslesen
sessionStorage.getItem('firstName') // Jessy

// Einträge löschen
sessionStorage.removeItem('firstName')
console.log(sessionStorage.getItem('firstName')) // null

// Alle Einträge löschen
sessionStorage.clear()

// save object
let user = {
    username: 'jessica'
}

// has to be a string
localStorage.setItem('userData', user)

console.log(localStorage.getItem('userData')) // [object, Object]

localStorage.setItem('userData', JSON.stringify(user))

console.log(localStorage.getItem('userData')) // { "username": "jessica" }

// Prüfen ob Storage verfügbar
if (typeof Storage != 'undefined') {
    console.log('Storage verfügbar')
    console.log(typeof Storage)
    console.log(typeof localStorage)
    console.log(Storage)
    console.log(localStorage)
}
		`, ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`
// Eigenschaften und Methoden des window-Objektes sind global verfügbar

console.log(innerWidth) // innere Breite des Browserfensters

console.log(innerHeight) // innere Höhe des Browserfensters

console.log(outerHeight) // inner Breite des Browserfensters

// ...
		`, ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`
// Beispiele für den Zugriff auf beliebige Elemente

// document.getElementById(id) id: string
// document.getElementsByClassName(class) class: string
// document.getElementsByName(name) name: string
// document.getElementsByTagName(tag) tag: string

// ab IE9 / Bug in iOS 8
// document.querySelector(css-syntax) // Element
// document.querySelectorAll(css-syntax) // Elementliste

// querySelector CSS-Syntax
('*')
('p')
('ul, ol')
('nav > ul > li')
('.class')
('#id')
('p.class') // <p> Tag mit Klasse ''
('[href]')
('[src='bild.png']')
		`, ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`
let headLine = document.querySelector('h1')

let paragraf = document.querySelector('p')

headLine.innerHTML = '<em>Hallo</em> Welt'

paragraf.innerText = 'Interessanter Paragraph'

let listItems = document.querySelectorAll('li')

// get all list items content
for(let i = 0; i < listItems.length; i++) {
    // guard
    if (!listItems[i]) {
        continue
    }
    console.log(listItems[i].innerText)
}

// Globales Selektieren
let myGlobalList = document.querySelector('#secondList')
let myOldList = document.getElementById('secondList')

// Lokales Selektieren
let items = myGlobalList.querySelectorAll('li')
let oldItems = myOldList.getElementsByTagName('li')
		`
	],
	'week2day2': [ ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 2 DAY 2
		`// IDL = Interface Description Language

let progressBar = document.querySelector('progress')
console.log(progressBar.value) // number
console.log(progressBar.mix)

// Beispielzuweisung
progressBar.value = 3
progressBar.max = 8
		`,
		`let myButton = document.querySelector('button')

// Boolsches Attribut über content-Attribut setzen
myButton.setAttribute('disabled', 'disabled')

// Boolsches Attribut entfernen
myButton.removeAttribute('disabled')

// Attribut toggeln
myButton.toggleAttribute('disabled')

// über IDL-Zugriff
myButton.disabled = false
		`,
		`let inpuField = document.querySelector('#eingabe')

// Content
console.log(inputField.getAttribute('value')) // 'Hey'
// IDL
console.log(inputField.value) // 'Hey'

inputField.setAttribute('value', 'Hey Du da')
console.log(inputField.getAttribute('value')) // 'Hey Du da'
console.log(inputField.value) // 'Hey Du da'

inputField.value = 'Hopp hopp'
console.log(inputField.getAttribute('value')) 'Hey Du da'
console.log(inputField.value) // 'Hopp hop'

inputField.onchange = function() {
    console.log(inputField.getAttribute('value')) // 'Hey Du da
    console.log(inputField.value) // user eingabe
}

/*
Vorteile von IDL-Attributen:

- Liefern genaue Datentypen
- einfachere Notation
- liefern exakte Werte bei Änderungen auf Interface-Ebene

Vorteile von Content-Attributen:

- Methoden => Arbeit mit Befehlsketten sind möglich
- Liefern zugrundeliegende Werte
- Ermöglichen Zugriff auf Attribute von Elementen in Dokumenten, die nicht auf Interface-Ebene verfügbar sind.

*/
		`,
		`let inputField = document.querySelector('#eingabe')

inputField.onfocus = function() {
    console.log('Fokus gesetzt')
}

inputField.onfocus = function() {
    console.log('Fokus erneut gesetzt?') // Der zweite Event-Handler überschreibt den vorherigen.
}

// Funtion auslagern

function focus() {
    console.log('Fokus gesetzt')
}
function moreFocusStatements() {
    console.log('Zusätzliche Info')
}
inputField.onfocus = focus

// oder beide Funktionen ausführen lassen
inputField.onfocus = function() {
    focus()
    moreFocusStatements()
}
		`,
		`window.onload = function() {
    console.log('DOM und verknüpfte Ressourcen wurden vollständig geladen!')
}

eventTarget.addEventListener('event', function() {
    // statements
})

// eventListener überschreiben sich nicht gegenseitig

// Beispiel mit Argument

function sagWas(meldung) {
    console.log(meldung)
}

eventTarget.addEventListener('focus', function() {
    sagWas('Hallo Welt!')
})
		`,
		`<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Klick mich</title>
    </head>
    <body> 
        <p>
            <button type="button">Klick mich!</button>
        </p>

        <div id="result"></div>

        <p>
            Klicke auf diese Schaltfläche, um den Zählerstand zu erhöhen.
        </p>
        <p>
            Schließe die Browser-Registerkarte (oder Browser-Fenster) und versuche es erneut. Der Zähler wird zurückgesetzt.
        </p>

        <script src="js/webstorage.js"></script>
    </body>
</html>`,
		`"use strict";

/*
    Die Klicks auf einen Button sollen gezählt und dem User zur Anzeige gebracht werden.
    In einem neuen Tab beginnt die Zählung von vorne.
*/

// benötigte Elemente refrenzieren
let klickButton = document.querySelector("button");
let ausgabe = document.querySelector("#result");

// Messages schreiben (Erfolg und Error)
const MESSAGE = " mal hast Du auf diesen blöden Button geklickt! Macht Dir das wirklich Spaß?";
const ERROR_MESSAGE = "Dein Browser kann leider nichts. Warte nicht! Mach was anderes!";

// Event-Handler für Button (click)
klickButton.addEventListener("click", clickFunction);

function clickFunction() {
    
    // prüfen, ob Storage genutzt werden kann (Guard)
    if(typeof Storage == "undefined") {    // wenn nein: 
        // Error ausgebn  
        ausgabe.textContent = ERROR_MESSAGE;
        // EventHandler deregistrieren 
        klickButton.removeEventListener("click", clickFunction);
        // Funktion des Handlers abbrechen 
        return;
    }

    let counter = 1;
    
    // wenn ja Klicks in Counter schreiben
    // Wert abfragen 
    let storage = sessionStorage.getItem("klicks");
    // - Eintrag vorhanden - erhöhen
    if(storage) counter = Number(storage) + 1;
    // - kein Eintrag vorhanden - setzen
    // Storage für beide möglichkeiten schreiben
    sessionStorage.setItem("klicks",counter);
    
    // Erfolgs-Message mit Anzahl der Klciks ausgeben
    ausgabe.textContent = counter + MESSAGE;
}
		`
	],
	'week2day3': [ ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 2 DAY 3
		`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>E-Book</title>
        <link
            rel="stylesheet"
            type="text/css"
            href="eBook.css"
            media="screen"
        />
    </head>

    <body>
        <div class="eBook_wrapper">
            <div class="eBook">
                <div class="eBook_content"></div>

                <div class="pagination">
                    <span class="goes_left">
                        <button title="first">«</button>
                        <button title="prev">‹</button>
                    </span>

                    <span class="goes_right">
                        <button title="next">›</button>
                        <button title="last">»</button>
                    </span>

                    <span class="progressbar">
                        <progress max="3" value="1" id="site_progress"></progress>
                    </span>

                    <span class="bookSite">0</span>
                </div>
            </div>
        </div>

        <script src="bookscript.js"></script>
    </body>
</html>`,
		`* {
    margin: 0;
    padding: 0;
}
body, html {
    font-size: 100%;
    background: #ddd;
    color: #333;
    font-size: 16px;
    font-family: Verdana, Arial, sans-serif;
}
p, h1, h2 {
    margin-bottom: 1rem;
    font-weight: normal;
}
h1 {
    font-size: 1.75rem;
}
h2 {
    color: rgb(29, 119, 29);
    font-size: 1.2rem;
    line-height: 1.4rem;
}
p {
    line-height: 1.2;
    -webkit-hyphens: auto;
    -moz-hyphens: auto;
    hyphens: auto;
}
p:last-child {
    margin-bottom: 3rem;
}
button {
    padding: 5px;
    font-size: 1.5rem;
}
.goes_left {
    float: left;
}
.goes_right {
    float: right;
}
.bookSite {
    font-size: .8rem;
    font-weight: bold;
    text-align: center;
    display: block;
    padding: .3rem;
    border: 2px solid #ccc;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    width: 20px;
    margin: 25px auto 0;
}
.eBook_wrapper {
    margin: 25px auto;
    width: 80vw;
    max-width: 800px;
    position: relative;
    background: #fff;
}
.eBook_wrapper .eBook {
    padding: 3rem 2rem 6rem 2rem;
    min-height: 75vh;
    position: relative;
    border: 1px solid #ccc;
    background-color: #fff;
    -webkit-box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);
    -moz-box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 3px rgba(0, 0, 0, 0.3);
}
.eBook_wrapper .eBook .eBook_footer {
    color: #999;
    font-size: .8em;
}
.eBook_wrapper .eBook .pagination {
    font-size: 1.8em;
    padding: 0 1.25rem;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 1rem;
    white-space: nowrap;
}
.eBook_wrapper .eBook .pagination .progressbar {
    display: block;
    width: 50%;
    margin: 0 auto;
}
.eBook_wrapper .eBook .pagination progress {
    width: 100%;
    height: 1rem;
    padding: 0;
    margin-bottom: .25rem;
    background-color: #ccc;
    -webkit-border-radius: 0.25rem;
    -moz-border-radius: 0.25rem;
    border-radius: 0.25rem;
    -webkit-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.4);
    -moz-box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.4);
    box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.4);
}
.eBook_wrapper .eBook .pagination progress::-webkit-progress-bar {
    padding: 0;
    border-style: none;
    background-color: transparent;
}
.eBook_wrapper .eBook .pagination progress::-webkit-progress-value {
    -webkit-border-radius: 0.25rem;
    border-radius: 0.25rem;
    background-color: #e445a7;
    background-image: -webkit-linear-gradient(top, #ddd, #e445a7, #ddd);
    background-image: linear-gradient(to bottom, #ddd, #e445a7, #ddd);
}
.eBook_wrapper .eBook .pagination progress::-moz-progress-bar {
    -moz-border-radius: 0.25rem;
    border-radius: 0.25rem;
    background-color: #e445a7;
    background-image: -moz-linear-gradient(top, #ddd, #e445a7, #ddd);
    background-image: linear-gradient(to bottom, #ddd, #e445a7, #ddd);
}

.eBook_wrapper .eBook .eBook_content:empty {
    color: #666;
    font-size: 1.25rem;
    text-align: center;
    padding-top: 12rem;
}
.eBook_wrapper .eBook .eBook_content:empty:after {
    content: "Keine Seite vorhanden.";
}`,
		`const BOOK_SITES = [
    "<h1>2.1 Einführung JavaScript und Webentwicklung</h1>" +
    "<h2>2.1.1 Der Zusammenhang zwischen HTML, CSS und JavaScript</h2>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>",

    "<h1>2.1 Einführung JavaScript und Webentwicklung</h1>" +
    "<h2>2.1.2 Das richtige Werkzeug für die Entwicklung</h2>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>",

    "<h1>Webseiten dynamisch verändern</h1>" +
    "<h2>5.1 Aufbau einer Webseite</h2>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>",

    "<h1>Webseiten dynamisch verändern</h1>" +
    "<h2>5.1.1 Document Object Model</h2>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>",

    "<h1>Webseiten dynamisch verändern</h1>" +
    "<h2>5.1.2 Die verschiedenen Knotentypen</h2>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>" +
    "<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>"
]

let progressBar = document.querySelector('#site_progress')

let eBookContent = document.querySelector('.eBook_content')

let bookSite = document.querySelector('.bookSite')

let buttons = document.querySelectorAll('button')

let currentSite = 1

let storage = localStorage.getItem('current')

// check storage
if (storage) currentSite = +storage

progressBar.max = BOOK_SITES.length

for (let i = 0; i < buttons.length; i++) {
    const button = buttons[i]
    button.addEventListener('click', changeSite)
}

function changeSite() {
    switch (this.title) {
        case 'first':
            currentSite = 1
            break
        case 'prev':
            currentSite--
            break
        case 'next':
            currentSite++
            break
        case 'last':
            currentSite = BOOK_SITES.length
            break
    }
    showSite()
}

function showSite() {
    bookSite.textContent = currentSite
    eBookContent.innerHTML = BOOK_SITES[currentSite - 1]
    progressBar.value = currentSite
    setButtonState()
    setLocalStorage()
}

function setLocalStorage() {
    if (typeof Storage) {
        localStorage.setItem('current', currentSite)
	}
}

function setButtonState() {
    for (let i = 0; i < buttons.length; i++) {
        if (buttons[i].title == "next" || buttons[i].title == "last") {
            buttons[i].disabled = currentSite == BOOK_SITES.length
        } else {
            buttons[i].disabled = currentSite == 1
        }
    }
}

// init
showSite()`,

		`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Font-Size ändern</title>
        <style>
            #biggest {
                font-size: 36px;
            }
            #big {
                font-size: 24px;
            }
            #normal {
                font-size: 16px;
            }
            #small {
                font-size: 11px;
            }
        </style>
    </head>

    <body>
        <button id="biggest">biggest</button>
        <button id="big">big</button>
        <button id="normal">normal</button>
        <button id="small">small</button>

        <p>Hier steht der Text, dessen Schriftgröße verändert wird</p>

        <script src="font-size.js"></script>
    </body>
</html>`,
		`let paragraf = document.querySelector('p')
const BUTTONS = document.querySelectorAll('button')
const UNIT = 'px'
const SIZES = { biggest: 36, big: 24, normal: 16, small: 11 }

for (let i = 0; i < BUTTONS.length; i++) {
    BUTTONS[i].addEventListener('click', setFontSizeTo)
}

function setFontSizeTo() {
    paragraf.style.fontSize = SIZES[this.id] + UNIT
}`,
		`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Font-Size schrittweise ändern</title>
        <style>
            p {
                font-size: 50px;
            }
            button {
                font-size: 24px;
                width: 32px;
                text-align: center;
            }
        </style>
    </head>

    <body>
        <button id="small">-</button>
        <button id="big">+</button>

        <p>Hier steht der Text, dessen Schriftgröße verändert werden soll</p>

        <script src="font-size.js"></script>
    </body>
</html>`,
		`const PARAGRAF = document.querySelector('p')
const STEP = 5
const UNIT = 'px'

let bigButton = document.querySelector('#big')
let smallButton = document.querySelector('#small')

// getComputedStyle(element)
const LOCAL_FONTSIZE = localStorage.getItem('fontSize')
if (LOCAL_FONTSIZE) {
    PARAGRAF.style.fontSize = LOCAL_FONTSIZE
} else {
    PARAGRAF.style.fontSize = getComputedStyle(PARAGRAF).fontSize
}

bigButton.addEventListener('click', () => {
    setFontSizeTo(getCurrentFontSize() + STEP)
})

smallButton.addEventListener('click', () => {
    setFontSizeTo(getCurrentFontSize() - STEP)
})

function setFontSizeTo(size) {
    PARAGRAF.style.fontSize = size + UNIT
    if (typeof Storage) localStorage.setItem('fontSize', size + UNIT)
}

function getCurrentFontSize() {
    let current = parseInt(PARAGRAF.style.fontSize)
    if (current > 45) current = 45
    if (current < 14) current = 14
    return current
}`
	],
	'week2day4': [ ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 2 DAY 4
		`// Syntax

// createElement
let element = document.createElement(tagName, [optionen])

// appendChild
let aChild = element.appendChild(aChild)

// cloneNode
let dupNode = node.cloneNode(deep)

// remove
Element.remove()

// removeChild
let oldChild = element.removeChild(child)

// ODER

element.removeChild(child)

// replaceChild
let replacedNode = parentNode.replaceChild(newChild, oldChild)

// replaceWith
element.replaceWith(...nodes)

// insertBefore
element.insertBefore(newNode, referenceNode)

// insertAdjacentElement
element.insertAdjacentElement(position, element)

// insertAdjacentHTML
element.insertAdjacentHTML(position, text)

// insertAdjacentText
element.insertAdjacentText(position, text)

// prepend
targetElement.prepend(...nodesOrDOMStrings)

// append
targetElement.append(...nodesOrDOMStrings)

// after
after(... nodes)

// before
before(... nodes)
		`,
		`if (!("remove" in Element.prototype)) {
    Element.prototype.remove = function() {
        if (this.parentNode) {
            this.parentNode.removeChild(this)
        }
    }
}
		`
	],
	'week2day5': [ ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 2 DAY 5
		`<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Einkaufsliste</title>
    <link rel="stylesheet" href="styles.css" />
</head>
<body>

    <h1>Mein elektronischer Einkaufszettel</h1>
    <table id="liste">
        <thead>
            <tr>
                <th>Name</th>
                <th>Anzahl</th>
                <!-- <th>Aktionen</th>-->
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Brot</td>
                <td>1</td>
                <!-- <td><button>X</button></td>--> 
           </tr>
        </tbody>
    </table>

    <p id="newItem">
        <input type="text" class="name" required placeholder="Artikel"/>
        <input type="number" class="anzahl" required min="1" max="999" placeholder="Anzahl"/>
        <button id="addItem">Setzen</button>
    </p>
    <script src="einkaufsliste.js"></script>
</body>
</html>`,
		`body,html {
    font-size: 16px;
    margin: 3rem;
}
input {
    margin-right: .4rem;
    padding: .5rem;
    width: 5rem;
    box-sizing: border-box;
}
.anzahl {
    width: 5rem;
    box-sizing: border-box;
}
#liste td:nth-child(2) {
    width: 11rem;
    box-sizing: border-box;
}
.name, #liste td:first-child {
    width: 15rem;
    box-sizing: border-box;
}
#addItem {
    padding: 0.5rem 1rem;
}
#liste {
    border-collapse: collapse;
}
#liste th,
#liste td {
    padding: .5rem 2rem;
    border: 1px solid #999;
}
#liste th {
    background-color: rgb(199, 230, 206);
    text-align: left;
}
#liste td > button {
    margin-right: 5px;
    font-size: 1.5rem;
    width: 2.5rem;
}
.removeItem {
    color: rgb(151, 43, 43);
}
.nachOben, .nachUnten {
    color: rgb(13, 80, 33);
}`,
		`// Referenzen auf benötigte Elemente im DOM erzeugen
let tableBody = document.querySelector("table#liste tbody");
let inputName = document.querySelector("input.name");
let inputAnzahl = document.querySelector("input.anzahl");
let addButton = document.querySelector("button#addItem");

let addItem = function () {
    // Elemente erzeugen
    // Tabellenzellen td erzeugen
    let itemTd = document.createElement("td");
    // value des text-inputs übergeben
    itemTd.textContent = inputName.value;
    let countTd = document.createElement("td");
    // value von number-input übergeben
    countTd.textContent = inputAnzahl.value;
    // Tabellenzelle für Aktionen erzeugen
    let actionsTd = document.createElement("td");
    actionsTd.append(createDeleteButton(), createButtonUp(), createButtonDown());
    // Tabellenzeile tr erzeugen
    let tableRow = document.createElement("tr");
    // DOM-Teilbaum erzeugen
    tableRow.append(itemTd, countTd, actionsTd);
    // Zeile in DOM integrieren
    tableBody.append(tableRow);
    updateButtonState();
}

// eventListener für addButton erzeugen
addButton.addEventListener("click", addItem);

// neue Spalte für Aktionen anlegen
// Spalte im thead erzeugen
let actionsTh = document.createElement("th");
actionsTh.textContent = "Aktionen";
document.querySelector("table#liste thead tr").appendChild(actionsTh);

// Spalte im tbody erzeugen
// alle tr-tags in NodeList selektiern
let allTableRows = tableBody.querySelectorAll("tr");
// NodeList durchlaufen und für jeden tr eine td hinzufügen
for (let i = 0; i < allTableRows.length; i++) {
    allTableRows[i].appendChild(document.createElement("td"));
}

// letzte Zellen in Zeilen selektieren
let lastCells = document.querySelectorAll("table#liste td:last-child");

// Action-Button in letzte Zellen integrieen
for (let i = 0; i < lastCells.length; i++) {
    lastCells[i].appendChild(createDeleteButton());
    lastCells[i].appendChild(createButtonUp());
    lastCells[i].appendChild(createButtonDown());
}

updateButtonState();

// ausgelagerte Funktionen
function createDeleteButton() {
    let button = document.createElement("button");
    button.textContent = "x";
    button.className = "removeItem";
    button.addEventListener("click", function () {
        // Zeile löschen, in der Button sich befindet
        this.parentNode.parentNode.remove();
        updateButtonState();
    });
    return button;
}

function createButtonUp() {
    let button = document.createElement("button");
    button.textContent = "↑";
    button.className = "nachOben";
    button.addEventListener("click", function () {
        // Zeile vor vorhergehende Zeile setzen
        // aktuelle Zeile
        let currentRow = this.parentNode.parentNode;
        // vorherige Zeile
        let prevRow = currentRow.previousElementSibling;
        prevRow.before(currentRow);
        updateButtonState();
    });
    return button;
}

function createButtonDown() {
    let button = document.createElement("button");
    button.textContent = "↓";
    button.className = "nachUnten";
    button.addEventListener("click", function () {
        // Zeile an Position nach nachfolgender Zeile verschieben
        // aktuelle Zeile 
        let currentRow = this.parentNode.parentNode;
        // folgende Zeile
        let nextRow = currentRow.nextElementSibling;
        nextRow.after(currentRow);
        updateButtonState();
    });
    return button;
}

// Übergabe von content (string), klasse (string) und action (string)
function createButton(content, klasse, action) {
    let button = document.createElement("button");
    button.textContent = content;
    button.className = klasse;
    let eventFunction;
    let currentRow = this.parentNode.parentNode;
    switch(action) {
        case "delete":
            eventFunction = function() {
                currentRow.remove();
                updateButtonState();
            };
            break;
        case "down":
            eventFunction = function() {
                currentRow.nextElementSibling.after(currentRow);
                updateButtonState();
            };
            break;
        case "up":
            eventFunction = function() {
                currentRow.previousElementSibling.before(currentRow);
                updateButtonState();
            };
    }
    button.addEventListener("click", eventFunction);
    return button;
}

function updateButtonState() {
    let allTableRows = tableBody.querySelectorAll("tr");
    for(let i = 0; i < allTableRows.length; i++) {
        allTableRows[i].querySelector("button.nachOben").disabled = !allTableRows[i].previousElementSibling;
        allTableRows[i].querySelector("button.nachUnten").disabled = !allTableRows[i].nextElementSibling;
    }
}
		`,
	]
}
