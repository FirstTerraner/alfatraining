export const codeBlocks = {
	'week3day1': [
		`let xmlString = '<?xml version="1.0" encoding="UTF-8"?>' +
'<artists>' +
'<artist name="Kyuss">' +
'<albums>' +
' <album>' +
' <title>Wretch</title>' +
' <year>1991</year>' +
' </album>' +
' <album>' +
'   <title>Blues for the Red Sun</title>' +
'    <year>1992</year>' +
'   </album>' +
'   <album>' +
'    <title>Welcome to Sky Valley</title>' +
'    <year>1994</year>' +
'  </album>' +
'  <album>' +
'   <title>...And the Circus Leaves Town</title>' +
'  <year>1995</year>' +
'  </album>' +
'  </albums>' +
'</artist>' +
'</artists>'

let myDomParser = new DOMParser()
let myXMLDoc = myDomParser.parseFromString(xmlString, 'text/xml')

let einArtist = myXMLDoc.querySelector('artist')

console.log(einArtist.getAttribute('name')) // Kyuss
// Zugriff über IDL nicht möglich weil Daten nur im Speicher vorhanden sind
console.log(einArtist.name) // undefined

// Nodelist
console.log(einArtist.querySelectorAll('title'))

let myXMLSerializer = new XMLSerializer()
let myXMLString = myXMLSerializer.serializeToString(myXMLDoc)
		`,
		`// Ablauf

/*
1. XMLHttpRequest-Object (XHR) erstellen
2. Event-Handler definieren => wartet auf Antwort vom Server
3. HTTP-Anfrage formulieren mit open()
4. Anfrage konfigurieren (kann entfallen)
5. Anfrage senden mit send()
*/

// 1.
let xhr = new XMLHttpRequest() // XHR Objekt
console.log('XHR: ', xhr)

// 2.
xhr.onload = function(){
    // Feuert, wenn Antwort vollständig geladen

    // Guard
    if (xhr.status != '200') { return }

    // Daten wurden ordnungsgemäß übermittelt
    console.log(xhr)
}

// 3.
xhr.open('GET', 'artists.xml')
// xhr.open('POST', 'script.php')

// 4. Konfigurieren:
// TYP: zB wie sollen Antwort-Daten vorliegen? (default: string)
// xhr.responseType = 'json' // geparste JS-Daten wenn vom Browser unterstützt 
xhr.responseType = 'document' // document object

// 5. Senden
xhr.send() // GET
// xhr.send([DATEN]) // POST

// Browser Fähigkeitenweiche in der Hoffnung es niemals anzuwenden. (:

function AJAXCheck () {
    var request = null
    if (window.XMLHttpRequest) {
        // moderne Browser und IE ab v7 
		request = new XMLHttpRequest()
    } else if(ActiveXObject) {
        // IE 6
        request = new ActiveXObject('Microsoft.XMLHTTP')
    }
    return request
}

if (AJAXCheck()) {
    var reuqest = AJAXCheck()
    request.onreadystatechange = function() {
        // Feuert bei jeder Änderung des readyStates (0-4)
		// mit readyState == 4 sind die Daten vollständig geladen
        if (request.readyState != 4) { return }
        if (request.status != 200) { return }
        // Daten eingetroffen
	}
} else {
	console.log('kein AJAX möglich')
}
		`
	],
	'week3day3': [
		`// Selektoren $() jQuery()

// Konfliktloser Modus: ('$' nicht mehr nutzbar)
jQuery.noConflict()

// Einleitender Handler für DomContentLoaded
$(document).ready(function() {})

$(function() {
    console.log($('#url_link')) // object

    // Attribute hinzufügen
    $('#url_link').attr('title', 'Ab jetzt mit jQuery')
    $('#url_link').attr({
        title: 'Ab jetzt mit jQuery',
        href: 'https://jquery.com'
    })

    // Attribute entfernen
	$('#url_link').removeAttr('title')

    // Zugriff auf Content ink. Markups auf den ERSTEN Element
    let contentH1 = $('h1').html()
    
    // ohne Markups
    let contentP = $('p').text()

    // Content setzen
	$('p').first().text('Hallo Welt!')

    // Auswahl, die eine Bedingung erfüllen
    $('p').has('ul')

    // Auswahl, die eine nicht Bedingung erfüllen
    $('p').not('ul')

    // Auswahl in einem Bereich
    $('p').slice(0, 3)
})
		`
	],
	'week3day4': [
		`// Content erzeugen

$("div.element").before("<p>before-Text</p>");
$("div.element").prepend("<p>prepend-Text</p>");
$("div.element").append("<p>append-Text</p>");
$("div.element").after("<p>after-Text</p>");

$("#email").after("<br><span class='fehler'>Mail-Adresse fehlerhaft</span>");

// Inline-Styles setzen und anliegende Styles lesen
console.log($("p").css("font-size"));   // 16px
$("p").css("background-color","blue");
console.log($("p").css("background-color"));
$("p").css({
    backgroundColor:    "orange",
    padding:            "10px 20px",
    border:             "1px solid green"
});

// Events

$("p").on("click", function() {
    alert("Da hat doch was geklickt");
});
$("p").on("click", function() {
    alert("Da hat's doch wirklich geklickt");
});
// mit off() alle mit on() registrierten Handler für ein übergebenes Ereignis deregistrieren
$("p").off("click");

// vordefinierte Effekte
$("#bild-ausblenden").on("click", function() {
    // Bild ausblenden
    $(".bild").hide(5000);
    return false;
});
$("#bild-einblenden").on("click", function() {
    // Bild einblenden
    $(".bild").show("slow");
    return false;
});

// eigene Effekte erstellen
$("button#animate").on("click",function() {
    $("div#container").animate({
        width:      "450px",
        fontSize:   "4em",
        opacity:    .5
    },2500);
});

// Navigation im DOM

// Kindelemente
console.log($("ul").children());        // Kindelemente
console.log($("article").find("a"));    // 1. Link im Artikel

// Elternelemente
console.log($("li").parent());          // direkte Elternelemente
console.log($("li").parents());         // alle Elternelemente
console.log($("li").parentsUntil("body"));  // alle Elternelemente bis exclusive dem übergebenen Element
console.log($("li").closest("article"));    // 1. Elternelement, dass einer Bedingung entspricht

// Geschwisterelemente
console.log($("h2").next());    // direkt folgendes Geschwisterelement
console.log($("h2").next("a")); // direkt folgendes Geschwisterlement, dass eine Bedingung erfüllt (muss direkt folgen)
console.log($("h2").nextAll()); // alle folgenden Geschwisterelemente
console.log($("h2").nextAll("a")); // alle folgenden Geschwisterelemente, die eine übergebene Bedingung erfüllen
console.log($("h2").nextUntil("a")); // alle folgenden Geschwisterelemente bis zu einem bestimmten Element (exclusive)

console.log($("ul").prev());    // direkt vorhergehendes Geschwisterelement
console.log($("ul").prevAll()); // alle vorhergehenden Geschwisterelemente
// weitere Methoden wie bei next()

console.log($("ul").siblings()); // alle Geschwisterelemente
		`
	]
}