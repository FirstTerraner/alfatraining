export const codeBlocks = {
	'week1day1': [ //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 1
		`<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Alfatraining Tag 1</title>
    </head>
    <body>
        <!-- Zum Beispiel am Ende des Body Containers -->
        <script src="./script.js" ></script>
    </body>
</html>
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Mein Kommentar auf einer Zeile

/*
    Mein Block Kommentar
    Wunderbar
*/

// Funktionshinweise
/**
 * @deprecated
 * @...
 */
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`<h1 id="hello"></h1>

<script>

// Helferfunktion
function getById(id) {
    if (!id) { return null }
    return document.getElementById(id)
}

let helloText = 'Hello World!'

// Begrüßung
getById('hello').innerText = helloText + ' Alfatraining Day 1.'

console.log(helloText + ' Alfatraining Day 1') // Hello World! Alfatraining Day 1.

</script>
` /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 2
	],
	'week1day2': [
		`/*
    Datentyp number
*/
Number.MAX_VALUE // größter wert in js
Number.MIN_VALUE // kleinster wert in js
const zahl = 5 // (number)
console.log(-1 / 0) // -Infinity (number)
console.log(0 / 0) // NAN not a number (number)
/*
    Datentyp String
*/
const myString = 'this is an example text'
/*
    Datentyp Boolean
*/
const allDayEveryDay = false
/*
    Datentyp Object
*/
const myObject = {} // empty
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Rückgabe immer String
console.log(typeof 5) // 'number'
console.log(typeof '') // 'string'
console.log(typeof boolean) // 'boolean'
console.log(typeof null) // 'object'
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Beispiele
console.log(Number('5')) // 5 (number)
console.log(+'    10      ') // 10 (number)
console.log(parseInt('42.54')) // 42
console.log(pasreInt('42 Jessy')) // 42
console.log(parseInt('16px')) // 16, Vorteil gegenüber Number()
console.log(parseInt(' 42 42')) // 42
` /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 3
	],
	'week1day3': [
		`/*

Relationale Operatoren

< kleiner als

> größer als // 1 Prüfung

<= kleiner oder gleich // 2 Prüfungen

>= größer oder gleich

== gleich // Datentyp wird implizit konvertiert

=== genau gleich // Datentyp bleibt bestehen

!= ungleich // implizit konvertiert

!== genau ungleich // typ bleibt bestehen

Rückgabe: boolean

*/

// BEISPIELE:
console.log(3 < 42) // true
console.log('3' < 42) // true
console.log('3' === 3) // false
console.log('3' == 3) // true

console.log('300' < '42') // true weil string vergleich zeichen für zeichen
console.log('Jessica' < 'Daredevil') // false
console.log('Jessica' < 'Jones') // true

console.log('3 Autos' < '4') // true
console.log('3 Autos' < 4) // false, da '3 Autos' => NaN

// fun
console.log(NaN === NaN) // false
console.log(NaN !== NaN) // true
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// if
if (true) {
    // DO IT!
    console.log('OK')
}

// short-if, only for SHORT statements
if (true) console.log('ziemlich kurz')
if (false) console.log('NO')
else console.log('YES')

// if else
if (true) {
    console.log('true')
} else {
    console.log('false')
}

//
if (true) {
    console.log()
} else {
    if (true) {
        console.log()
    } else[
        console.log()
        ...
    ]
}

// if, else if
if (true) {
    console.log()
} else if (true) {
    console.log()
}
// ...

// TIPP : immer vom Speziellen zum Allgemeinen

// Mehr Beipiele:

let userResp = prompt('Die Antwort auf alle Fragen: ')
const CORRECT = 42
if (userResp == 42) {
    console.log('top')
} else {
    console.log('versuchs nochmal')
}

// einfache Prüfungen
if (null) console.log(true, 'bei null')
else console.log(false, 'bei null')

// if (null) => false
// if ('') => false
// if (0) => false
// if ('0') => true
// if (' ') => true
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// || => Oder
// ergibt nur false, wenn alle Bedingungen false - sonst true

// true || false  => true
// False || true => true
// true || true => true
// false || false => false

// && => Und
// ergibt nur true, wenn alle Bedingungen erfüllt sind, sonst false
// true && false  => false
// False && true => false
// true && true => true
// false && false => false

// ! => Not operator
// wandelt true in false und false in true

// !true => false
// !false => true

let a = 1, b = 20
if (a < 2 || b > 10) {
    console.log('beide bedingungen sind wahr')
} else { console.log('insgesamt unwahr') }
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Kleine Beispiele:

let browser = 'Mozilla'
console.log(browser.substring(0, 3)) // 'Moz'
console.log(browser.substring(3, 0)) // 'Moz'
let src = 'https://blabla.com/46854354888852.jpg'
console.log(src.substring(src.length - 3)) // jpg

let name = 'Jessica Jones'
let spacePosition = name.indexOf(' ')
let firstname = name.substring(0, spacePosition) // Jessica
let lastName = name.substring(spacePosition + 1)
console.log('Vorname: ' + firstname, 'Nachname: ' + lastName) // Vorname: Jessica, Nachname: Jones

let user = '  jessica  '
console.log(user.trim()) // Jessica
console.log(user.trim().charAt(0).toUpperCase() + user.trim().substring(1)) // Jessica
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Beispiele:

const stringArray = ['', '', ... ]

const numberArray = [1, 2, ... ]

const randomArray = [
    '1',
    2,
    {value: 3, array: ['3', '4']},
    true
]

const nestedArray = [['1', '2', ...], ['1', '2', ...], ... ]
` /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 4
	],
	'week1day4': [
		`for (let i = 0; i < 11; i++) {
    console.log(i + ' zum Kuckuck')
}

for (let i = 0; i < 11; i++) {
    console.log('Quadrat von ' + 'i ' + 'ist: ' + (i * i))
}

// Mehrere Konditionen

for (let i = 0, j = '0'; j.length < 15 && i < 15; i++, j += 1) {
    console.log('i: ' + i, 'j: ' + j, 'J Länge: ' + j.length)
    console.log(j.length < 15)
}

// Fußgesteuerte do..while-Schleife

do {
    console.log('while loop')
} while (a === b)
	
const SOLUTION = 42
let answer

do {
    answer = prompt('Die Antwort auf alle Fragen...')
    if (answer) answer = answer.trim()
} while(answer != SOLUTION)
	
// Kopfgesteuerte while-Schleife

while(a === b) {
    console.log('while loop')
}

let username = '   Jessica   Lena         Jones'
while(username.includes('  ')) {
    let nameFirst = username.substring(0, username.indexOf('  ') + 1)
    let nameSecond = usename.substring(username.indexOf('  ')).trim()
    username = nameFirst + nameSecond
    console.log(username) // 'Jessica Lena Jones'
}`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`
// beenden mit break;
// beenden mit continue (den aktuellen durchlauf)

for (let i = 0; i < 11; i++) {
    let result = i * i + 42
    console.log(result)
    if (result > 100) {
        break;
    }
    if (result % 2) {
        console.log('unterbrechung')
        continue
    }
    console.log(ergebnis)
}

// Hinweis:
// in for-Schleifen springt der Interpreter zur Schlussanweisung
// in while-Schleifen springt Interpreter zur Bedingungsprüfung
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Funktionsdeklaration

function Bezeichner(parameter) {
    // statements
    return Rückgabewert
}
// Aufruf
Bezeichner('OK')

// Parameter und return optional

function myFunc() {
    // statements
}

// kann vor Deklaration aufgerufen werden, unterleigt dem Hoisting

myFunc()
function myFunc() {
    console.log('Guten Morgen')
}
myFunc()

// mit Parametern

function logMessage(message) {
    console.log(message)
}
logMessage('Hey') // Hey
logMessage(42) // 42
logMessage() // undefined

function moreParams(message, anzahl) {
    // statements with parameters
    console.log(message + ' - ' + anzahl)
    console.log(message.repeat(anzahl))
}
moreParams('Hello', 5)

// mit Rückgabewert

function withReturn(message) {
    const returnMsg = message + ' new word'
    return returnMsg
    // short: return message + ' new word'
}
console.log(withReturn('Hello'))

function product(a, b) {
    console.log(a)
    console.log(b)
    return a * b
}
console.log(product(2, 3)) // 6
console.log(product(3, 4)) // 12
console.log(product(2)) // NaN
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`console.log(bezeichner('hello')) // Reference-Error
let bezeichner = function(parameter) {
    // statements
    return parameter
}
bezeichner('Hello') // Hello

let square = function(a) {
    return a * a
}
console.log(square(3)) // 9
console.log(square()) // NaN

// Default-Werte für Parameter

function wellcomeUser(username='anonym', title='...') {
    console.log('Hey, ', + title + ' ' + username)
}
wellcomeUser() // Hey, ... anonym
welcomeUser('newUser', 'main') // Hey, main newUser

function multiply(a = 0, b = 1, c = 1, d = 1, e = 1) { // optional arguments for calculation
    console.log(a * b * c * d * e)
}

console.log(multiply()) // 0
console.log(multiply(5)) // 5
console.log(multiply(5, 3))	// 15
console.log(multiply(2,2,2,2,2,2,2,2,2,2)) // additional paramters are ignored
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Helferfunktion
let divide = function (a, b) {
    // guard against bad inputs
    if (a == 0 || b == 0) { return 0 }
    return a / b
}
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`var zahl = 11
(function() {
    var inline = 42
    console.log(inline + zahl)
})(); // 53
console.log(zahl + inline) // Reference-Error

var result = (function(a, b) {
    var ergebnis = a + b
    return ergebnis
})(2, 3);

console.log(result) // 5
console.log(result()) // type-Error

// kurzschreibweise (unterstützt kein return):
!function() {
    console.log('Hallo Welt aus der IIFE')
}();
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`let leeresArray = []

console.log(leeresArray) // []

// Zahlen array
let numberArray = [1, 2, 3, 4, 5, 6]

console.log(numberArray) // [1, 2, 3, 4, 5, 6]

console.log(leeresArray.length) // 0

// Erzeugung über Kontruktor-Funktion
let myNewArray = new Array([])

// random array
let randomArray = [1,2,true,'Jessica', 5]

// string array
let heroes = ['Jessica Jones', 'Luke Cage', 'Daredevil']
console.log(heroes.length) // 3

// property access
console.log(heroes[0]) // 'Jessica Jones'

// access last property in unknown length
console.log(heroes[heroes.length - 1])

let sparseArray = [1,,3]
console.log(sparseArray[1]) // undefined

let allArrays = [numberArray, randomArray, heroes, sparseArray]

// Array Schleife
for(let i = 0; i < numberArray.length; i++){
    console.log(numberArray[i]) // log array properties
    // do something with numberArray[i]

    // nested loop
    for (let j = 0; j < heroes.length; j++) {
        // ...
    }
}
` /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// WEEK 1 - DAY 5
	],
	'week1day5': [
		`// Kleine Beispiele:

let heldenOnline = ["Jessica", "Hulk", "Loki", "Odin", "Thor", "Nebula", "Elektra"]
console.log(heldenOnline(indexOf('Jessica'))) // 0

// Beachten:
console.log(heldenOnline(indexOf('Jess'))) // -1

// 2. Argument:
console.log(heldenOnline.indexOf('Jessica', 2)) // -1

// includes
console.log(heldenOnline.includes('Jessica')) // true
console.log(heldenOnline.includes('Jessica', 2)) // false

// concat
let array_1 = [1,2,3,4,5], array_2 = [6,7,8,9]

let array_3 = array_1.concat(array_2)
console.log(array_3) // [1,2,3,4,5,6,7,8,9]
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Erstellung über Literal-Schreibweise

let myObject = {} // leeres Objekt

// Erstellung über Konstruktor-Funktion

let myNewObject = new Object() // leeres Objekt

// Objekt mit Inhalt

let book = {
    mainTitle: 'JavaScript',
    subTitle: 'Lerne programmieren',
    targetGroup: 'Anfänger und Fortgeschritten',
    price: 39.95,
    'Händer-Preis': 19.95,
    showTitle: function() {
        console.log('lets go ' + this.mainTitle)
    }
}

// Eigenschaft auslesen
console.log(book.mainTitle) // 'JavaScript'
console.log(book.price) // 39.95

// string eigenschaft auslesen
console.log(book['Händer-Preis']) // 19.95

// Eingenschaft ändern
book.mainTitle = 'Python'
console.log(book.mainTitle) // 'Python'

// Eigenschaften hinzüfungen
book.inStock = 499
book.removeFromStock = function(count) {
    if (book.inStock < count) {
        console.log('Ausverkauft oder nicht genügend Stückzahl')
        return
    }
    book.inStock -= count
}
book.removeFromStock(5)
console.log(book.inStock) // 494

// Eigenschaft löschen
delete book.inStock

let shoppingCart = [book, ...]
// oder
let shoppingCart = []
shoppingCart.push(book)

let customer = {
    name: '',
    age: 18,
    address: 'Muster Straße 3'
}

function buyNow(shoppingCart, customer) {
    // statements
}

buyNow(shoppingCart, customer)
`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`// Das Date Objekt und seine Eigenschaften

console.log(Date)

// Aktuelles Datum
let heute = new Date()

// Aktuellen UNIX Zeitstempel (Millisekunden seit 01.01.1970)
console.log(Date.now())

// Bestandteile des Datums ändern
let myDay = new Date()
myDay.setFullYear(2222)
myDay.setMonth(11)
myDay.setDate(24)
myDay.setHours(20)
// ...

// Mit Datum rechnen
myDay.serDate(myDay.getDate() - 1) // gestern

// Bestimmtes Datum erzeugen
console.log(new Date('Tue Dec 24 2222 20:30:00 GMT+0100'))

// required: year, month
// optional: day, hour, minute, second, ms
let eventDate = new Date(2022, 2, 24, 12, 15, 30)
console.log(eventDate)

let dateUnix = new Date()

console.log(dateUnix.setTime(16330895665))
console.log(new Date(16330895665))

console.log(new Date().toLocaleString('de-DE'))
console.log(new Date().toLocaleString('de-DE'), {month: 'long'})

// option properties optional
const OPTIONS = {
    weekday: 'long',
    month: 'long',
    day: 'numeric',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit',
    second: '2-digit'
}

console.log(new Date().toLocaleString('de-DE'), OPTIONS)

`, ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		`let productArray = [
    {
        name: 'Single Malt',
        category: ['office drinks', 'alcohol'],
        inStock: 25,
        price: 29,95
    },
    {
        name: 'Shivas Regal',
        category: ['alcohol'],
        inStock: 50,
        price: 39,95
    },
]

// Daten serialisieren
let JSONString = JSON.stringify(productArray)
console.log(JSONString)

// Daten parsen
let parsedJSON = JSON.parse(JSONString)
console.log(parsedJSON)

// Weitere Beispiele:
console.log(JSON.stringify(42))
console.log(JSON.stringify(true))
`,
		`
// Zuweisen

document.cookie = 'user=Jessica'

document.cookie = 'user=Jessica Jones;path=/admin'

document.cookie = 'accept=1;max-age=86400;sameSite=None;Secure'
` ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	]
}