export const codeBlocks = {
	'week5day1': [
		'<div id="katze"></div>',
		`let element = document.querySelector('#katze')
let counter = 0, animationId

// Element styling
element.style.position = 'relative'
element.style.width = '100px'
element.style.height = '100px'
element.style.backgroundColor = 'blue'

// Rekursiver Aufruf
function runCat() {
    let position = counter * counter / 8
    counter++
    cat.style.left = position + 'px'
    if (counter <80) {
        animationId = requestAnimationFrame(runCat)
    } 
}
requestAnimationFrame(runCat)
		`,
		`let heroes = ['Jessica', 'Luke', 'DareDevil']

for (let heroIndex in heroes) {
    console.log(heroIndex, heroes[heroIndex])
}
let user = {
    firstName: 'Jessica',
    lastName: 'Jones'
}

// Bei Objekten kann es passieren, dass auch geerbte Eigenschaften
// geliefert werden (Prototyping)
let myObjectKeys = Object.keys()
for (let i = 0; i < myObjectKeys; i++) {
    console.log(myObjectKeys[i], user[myObjectKeys[i]])
}

// Iteration über Eigenschafswert (value-list)
for (let hero of heroes) {
    console.log(hero, heroes.indexOf(hero))
}
// Funktioniert nicht über einfache Objekte, da diese keinen eigenen Iterator haben
// und müssen Iterator extra übergeben:
for (let key of Object.keys(user)) {
    console.log(key)
}
for (let value of Object.values(user)) {
    console.log(value)
}
for (let entry of Object.entries(user)) {
    console.log(entry)
}

// Objekte einfrieren -> read-only
Object.freeze(user)
user.firstName = 'Hulk' // Uncaught TypeError: "firstName" is read-only
user.age = 38 // TypeError: can't define property "age": Object is not extensible

// Außerhalb des strikten Modus -> keine Fehlermeldung (still übergangen)
// AUch dann sind keine Objekt-Änderungen möglich.

let cat = 'Lucy'
let dog = 'Puffy'
let hero = 'Jessica'
// ES5
let myNewObj = {
    cat: cat,
    dog: dog,
    hero: hero
}
// ES6
let myObj = {
    cat,
    dog,
    hero
}
		`,
		`let myFormData = new FormData()
myFormData.append('firstname', 'Jessica')
myFormData.append('lastname', 'Jones')

let myForm = document.querySelector('#myForm')
myForm.onsubmit = function() {
    let formData = new FormData(myForm)
    // erweitern, ergänzen eines Formulares
    
    // ohne Prüfung auf das Vorhanden des Elementes möglich
    formData.delete('unsinn')

    // Durch alle keys iterieren und das selbe Spiel mit FormData.values() und FormData.entries()
    for (let key of formData.keys()) {
        console.log(key)
    }

    // Absenden des Formulares unterbinden
    return false
}
		`,
		`let obj = {}
obj.id = 99
let id = Symbol('id')
obj[id] = 42
console.log(obj)
		`

	],
	'week5day2': [
		`// Kontruktor
let regEx = new RegExp("abcd")

// Literale Schreibweise
let regEx = /abcd/

// Kleine Beispiele
let myString = 'Jessica Jones und Luke sind Helden aus dem Marvel-Universum'
let myRegEx = /essie/
let myRegEx2 = /uce/
console.log(myRegEx.test(myString)) // true
console.log(myRegEx2.test(myString)) // false

// flag g (global, alle vorkommen finden)
let myRegExFlag_g = /abcde/g
// flag i (ignore, Groß/Kleinschreibung ignorieren) 
let myRegExFlag_i = /abcde/i
// flag m (multiline, wenn Zeichenkette über mehrere Zeilen erstreckt)
let myRegExFlag_m = /abcde/m

/*
Ausdruck        Bedeutung

.               Beliebiges Zeichen (kein Zeilenumbruch)
u               das Zeichen "u"
essie           die Zeichenkette "essie"
a|b             das Zeichen "a" oder "b"
*/

// Test soll mindestens 9 Zeichen lang sein
let regExp = /........./ 
console.log(regExp.test("Jessica Jones")) // true
console.log(regExp.test("Jessy")) // false

// Zeichenklassen nehmen den Bezug auf einzelnes Zeichen

// Einfache Klasse: [xyz] -> entweder x oder y oder z

// Vorgeschaltetes Zirkumflex erzeugt Negation
// Negation: [^xyz] -> kein x, y oder z

// Bereiche: [a-zA-Z] -> alle Zeichen zwischen a und z, A und Z

// Beispiel für Suchmuster: "Xxxxxx Xxx"
let regEx_10 = /[A-Z][a-z][a-z][a-z][a-z][a-z] [A-Z][a-z][a-z]/

// Vordefinierte Zeichenklassen

// \\w           Wortzeichen
// \\W           Kein Wortzeichen
// \\s           Leerzeichen
// \\S           Kein Leerzeichen
// \\d           Ziffer zwischen 0-9
// \\D           Kein Ziffer

// Vereinfachter Suchmuster: "Xxxxxx Xxx"
let regEx_10_short = /[A-Z]\\w\\w\\w\\w\\w [A-Z]\\w\\w/

// Positionierung / Begrenzung

// ^             Anfang einer Zeichenkette
// $             Ende einer Zeichenkette
// \\b           Anfang oder Ende eines Wortes
// \\B           Keine Wortgrenze

// Beispiel Zeichenkette soll mit "J" beginnen
let regEx_J = /^J/
console.log(regEx_J.test("Luke")) // false

// Beispiel für eine Gruppierung:

let myDateRegex = /^(\\d{4})-(\\d{2})-(\\d{2})/
let result = myDateRegex.exec("2022-02-22 11:50")
console.log(result[0]) // 2022-02-22
console.log(result[1]) // 2022
console.log(result[2]) // 02
console.log(result[3]) // 22
console.log(result[4]) // 0 (index)
		`
	],
	'week5day3': [
		`// Beispiel:
let add = function (a, b) {
    return a + b
}
// Pfeilfunktion und implizites Return
let add_short = (a, b) => a + b

let showText = function(text) {
    return 'it is my ' + text
}
// Kein Klammern nötig bei einem einzigen Argument
let showText_short = text => 'it is my ' + text

// Pfeilfunktion im block, benötigt ein Return Statement
let showText_block = text => {
    if (!text) { return '' }
    return 'it is my ' + text
}

const materials = [
    'Hydrogen',
    'Helium',
    'Lithium',
    'Beryllium'
]
console.log(materials.map(material => material.length))
// output: Array [8, 6, 7, 9]

let person = {
    name: 'Jessica',
    age: 36,
    myMethod() {
        console.log(this)
    }
}
person.myMethod() // output: {name: "Jessica", age: 36, myMethod: myMethod()}

function test () {
    console.log(this)
}
test() // undefined
let user = {
    name: 'Jessica',
    test,
    myMethod() {
        let myFunc = function() {
            console.log(this)
        }
        myFunc()
    }
    myArrow() {
        let myFunc = () => console.log(this)
        myFunc()
    }
}
user.test() // {name: "Jessica", test: test()}
user.myMethod() // undefined, da "this" aus dem Kontext fällt
user.myArrow() // Objekt Referenz ist gegeben
		`,
		`// Funktionsdeklaration
function hey () {
    return "Hey!"
}

// Funktionsausdruck
// Anonyme Funktion, die einer Variable zugewiesen wird.
let hey = function() {
    return "Hey!"
}

// Pfeilfunktion
let arrow = () => "Hey!"

// Funktionen als Argumente
let argFunc = (arrow) => arrow()

let heldenOnline = ['Odin', 'DareDevil', 'Hulk', 'Jessica']
// Sortieren anhand der ASCII-Tabelle
console.log(heldenOnline.sort()) // Alphabetisch sortierte Helden

// Selbst erzeugte Sortierungsfunktion als callback übergeben für Array.sort()
// negatives ergebnis = muss nicht sortieren
// positives ergebnis = muss sortieren
let zahlenArray = [1,8,5,4,7,7,5,2]
console.log(zahlenArray.sort((a, b) => a - b))
		`,
		`let obst = ['Apfel', 'Birne', 'Banane']
let gemüse = ['Karotte', 'Brokolli']
let joined = [...obst, ...gemüse]
		`
	],
	'week5day4': [
		`// Beispiel Argumente:
let heroInfos = function (firstname, lastname, address) {
    console.log({
        firstname,
        lastname,
        address
    })
    console.log(arguments)
}
heroInfos('Jessica', 'Jones', 'HellStreet 66')

// Beispiel REST-Argumente
let restFunc = function (firstname, lastname, address, ...params) {
    // REST-Parameter 
    console.log(params)
}
restFunc('Jessica', 'Jones', 'HellStreet 66', 36, 68)

let add = function (...params) {
    let sum = 0
    params.forEach(param => sum += param)
    return sum
}
add(3,4)
add(3,9,88,4,5,6,46,547,16,557,136,58)
		`,
		`let body = document.body

// Reaktion auf die aufsteigende Phase (Bubbeling)
body.addEventListener('click', function() {
    console.log('Body hat die Bubbeling Phase mitbekommen')
})

// Reaktion auf die absteigende Phase (Capturing)
// (Wird zuerst abgefeuert, vor dem aufsteigenden Event)
body.addEventListener('click', function() {
    console.log('Body hat die Capturing Phase mitbekommen')
}, true) // "true" ermöglicht das Reagieren auf die Capturing Phase
`,
		`// Factory Function, um Objekte zu erzeugen

// Die Pfeilfunktion hat einen impliziten Return, da eine Funktionsklammern genutzt werden 
const createUser = ({username, avatar}) => ({
    username,
    avatar,
    setUsername(newUsername) {
        this.username = newUsername
        return this
    }
})

// Object.create() Beispiel
let whisky = {
    name: '',
    category: ['Whisky'],
    price: 0,
    inStock: 500,
    shortPrint() {
        console.log({
            category: this.cat[0],
            name: this.name,
            price: this.price
        })
    },
    slogan(slogan) {
        console.log(slogan)
    }
}

// Vererbte Eigenschaften vom Prototypen
let newProduct = Object.create(whisky)
newProduct.name = 'Shivas Regal'
newProduct.category.unshift('Office drink')
newProduct.price = 29.99
newProduct.slogan('Shivas Regal - Macht Sie alle prall')
// Neue Eigenschaften und Methoden hinzufügen 
newProduct.limit = 1000
`
	],
	'week5day5': [
		`// Klassendeklaration
class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    formSizes() {
        return 'width: ' + this.width + ' * ' + 'height: ' + this.height
    }
    newHeight(height) {
        this.height = height
    }
}

let newForm = new Form(50, 50)

newForm.newHeight(100)

console.log(newForm.formSizes())
		`,
		`class Form {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    get formSizes() {
        return 'width: ' + this.width + ' * ' + 'height: ' + this.height
    }
    get square() {
        return Math.pow(this.height, 2)
    }
    set newHeight(height) {
        this.height = height
    }
    set addWidth(width) {
        this.width += width
    }
}

let newForm = new Form(50, 50)

newForm.newHeight = 100

console.log(newForm.formSizes)
		`,
		`class Rectangle {
    constructor(height, width) {
        this._height = height;
        this._width = width;
    }
    formSizes() {
        return 'width: ' + this._width + ' * ' + 'height: ' + this._height
    }
    newHeight(height) {
        this._height = height
    }
}
		`,
		`class Rectangle {
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    formSizes() {
        return 'width: ' + this.width + ' * ' + 'height: ' + this.height
    }
    newHeight(height) {
        this.height = height
    }
}

class RectangleDescription extends Rectangle {
    constructor(height, width, name) {
        super(height, width)
        this.name = name;
    }
}
		`,
		`class Whisky {
    constructor(name, art, age, price) {
        this._name = name
        this._art = art
        this._age = age
        this._price = price
    }
    static GET_WHISKY_ART() {
        return {
            SL: 'Scotch',
            BB: 'Bourbon',
            IRE: 'Irish'
        }
    }
    get name() {
        return this.name
    }
    set name(newName) {
        this.name = newName
    }
}

let newWhisky = new Whisky('Glenfiddich', Whisky.GET_WHISKY_ART().IRE, 25, 49.99)
		`,
		`const DOG_NAME = Symbol("dogname");

class Dog {

    constructor(name) {
        this[DOG_NAME] = name
    }

    get name() {
        return this[DOG_NAME]
    }

    set name(newName) {
        this[DOG_NAME] = newName
    }
}

let myDog = new Dog("Snoopy")
myDog.name = "Leika"
console.log(myDog)
console.log(myDog.name)
		`
	]
}