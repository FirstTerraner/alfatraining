import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'

export default function Week6ay4() {
	return (
		<>
			<span>Datum: 03.03.2022</span>
			<Topic
				header='Fetch API'
				refLinks={[{
					url: 'https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch',
					description: 'Fetch Dokumentation'
				}]}
			/>
			<Topic
				header='EJS Template Engine'
				refLinks={[{
					url: 'https://ejs.co/',
					description: 'EJS Template Seite'
				}]}
			/>
			<Topic
				header='Theoretische Einführung in Datenbanken'
				list={['SQL, NoSQL, Sequelize, SQLite']}
				refLinks={[
					{
						url: 'https://sequelize.org/',
						description: 'https://sequelize.org/'
					},
					{
						url: 'https://sequelize.org/v5/manual/data-types.html',
						description: 'Sequelize Data types'
					}
				]}
			/>
			<DayJumperButtons WEEK='6' DAY='4' />
		</>
	)
}