import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'

export default function Week6Day2() {
	return (
		<>
			<span>Datum: 01.03.2022</span>
			<Topic
				header='Backend Entwicklung mit NodeJS'
				list={[
					'Theoretische Einführung in die NodeJS Umgebung',
					'CSV Dateien, Comma Separated Values',
					'Node Package Manager (npm)',
					'Terminal Befehler wie: cd, ls, dir, mkdir, node (REPL -> read, print, loop), .exit, npm'
				]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Learn/Server-side/Express_Nodejs',
					description: 'Express Web Anwendungsrahmen (Node.js/JavaScript)'
				}]}
			/>
			<Topic
				header='Datei System Bibliothek (fs)'
				list={[
					'fs.readFileSync()',
					'fs.readFile() mit callback',
					'fs.writeFileSync()',
					'fs.writeFile() mit callback',
					'fs.createReadStream() und Stream Event handler (on("data"))',
					'pipe()'
				]}
				refLinks={[{
					url: 'https://nodejs.org/api/fs.html',
					description: 'File System Dokumentation'
				}]}
			/>
			<Topic
				header='Datei-Komprimierungsbibliothek (zlib)'
				list={[
					'zlib.gzipSync()',
					'zlib.gzip() mit callback'
				]}
				refLinks={[{
					url: 'https://nodejs.org/api/zlib.html',
					description: 'Zlib Dokumentation'
				}]}
			/>
			<Topic
				header='Node Package Manager'
				list={[
					'package.json Datei und Inhalt',
					'NPM Befehle wie: npm.list, npm.search "name" ',
					'Die Bibliothek validator.js',
					'validator.isISBN()',
					'Eigene Module erstellen',
					'exports'
				]}
				refLinks={[
					{
						url: 'https://npmjs.com',
						description: 'npm packages'
					},
					{
						url: 'https://www.npmjs.com/package/validator',
						description: 'Validator.js Dokumentation'
					}
				]}
			/>
			<DayJumperButtons WEEK='6' DAY='2' />
		</>
	)
}