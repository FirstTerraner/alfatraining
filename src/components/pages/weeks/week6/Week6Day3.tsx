import Topic from '../../../Topic'

export default function Week6ay3() {
	return (
		<>
			<span>Datum: 02.03.2022</span>
			<Topic
				header='NodeJS Entwicklung'
				list={['Eigene Module erstellen', 'HTTP-Server', 'Express-Server', 'Static web', 'Body-parser', 'Fetch API']}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Learn/Server-side/Express_Nodejs',
					description: 'Express Web Anwendungsrahmen (Node.js/JavaScript)'
				}]}
			/>
		</>
	)
}