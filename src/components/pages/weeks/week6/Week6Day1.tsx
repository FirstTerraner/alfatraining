import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek6'

export default function Week6Day1() {
	return (
		<>
			<span>Datum: 28.02.2022</span>
			<Topic
				header='Destrukturieren'
				list={['Arrays und Objekte destrukturieren', 'Werte aus mehrdimensionalen Arrays extrahieren']}
				codeLanguage='javascript'
				code={codeBlocks['week6day1'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment',
					description: 'Destrukturierende Zuweisung'
				}]}
			/>
			<Topic
				header='Datenstruktur Map'
				list={[
					'Mit ES6 in der Spezifikation aufgenommen',
					'Geordnete Sammlung von Schlüssel-Wert-Paaren',
					'Objekte in Maps umwandeln und umgekehrt',
					'Arrays in Maps umwandeln und umgekehrt',
					'Map-Schlüssel akzeptiert jeden Datentyp als Schlüssel',
					'Schlüssel muss eindeutig sein',
					'Map Methoden Beispiele: set(key, value), get(entry), has(entry), delete(entry), clear(), keys(), values(), entries(), forEach()'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week6day1'][1]}
				refLinks={[{
					url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map',
					description: 'Map Referenz'
				}]}
			/>
			<Topic
				header='Datenstruktur Set'
				list={[
					'Set: Sammlung von eindeutigen Werten',
					'Set Methoden Beispiele: add(entry), has(entry), delete(entry), clear(), values(), forEach()',
					'Duplikate aus einem Array entfernen mit new Set(array)',
				]}
				codeLanguage='javascript'
				code={codeBlocks['week6day1'][2]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Set',
					description: 'Set Referenz'
				}]}
			/>
			<Topic
				header='Iteratoren und Generatoren'
				list={[
					'iterator.next()',
					'Eigenen Iterator erstellen',
					'Generator Funktion',
					'generator.next()',
					'Der yield operator',
					'Weitere Aufrufe von next() erzeugen Fehlermeldung in einigen Browsern',
					'Über Generatoren iterieren'
				]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators',
						description: 'Dokumentation über Iteratoren und Generatoren'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/yield',
						description: 'Der yield Operator'
					}
				]}
			/>
			<DayJumperButtons WEEK='6' DAY='1' />
		</>
	)
}