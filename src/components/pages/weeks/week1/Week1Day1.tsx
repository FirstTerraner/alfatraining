import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek1'

export default function Week1Day1() {
	return (
		<>
			<span>Datum: 24.01.2022</span>
			<Topic
				header='Einführung in Visual Studio Code'
				list={['Erweiterungen', 'Shortcuts', 'Dateien und Ordner anlegen']}
				refLinks={[
					{
						url: 'https://code.visualstudio.com/docs',
						description: 'Visual Studio Code Dokumentation'
					}
				]}
			/>
			<Topic
				header='Das Einbinden von Skripten'
				list={['HTML Grundgerüst', 'Herkömmliche DOM Tags']}
				codeLanguage='html'
				code={codeBlocks['week1day1'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/HTML/Element/script',
						description: 'Lerne mehr über den script Container'
					},
					{
						url: 'https://www.w3schools.com/TAGS/default.ASP',
						description: 'Finde alle verfügbaren HTML Container Bezeichnungen'
					}
				]}
			/>
			<Topic
				header='JavaScript Schreibregeln Beispiele'
				list={['Case-sensitive', 'Index Zählung beginnt bei 0', 'Kommentare']}
				codeLanguage='javascript'
				code={codeBlocks['week1day1'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Grammar_and_types',
						description: 'Lerne mehr über JavaScript Regeln'
					}
				]}
			/>
			<Topic
				header='JavaScript Konventionen'
				list={[
					'Ein Statement pro Zeile. Optionale Semikolon Nutzung, bis auf wenige Pflichtfälle.',
					'Klare Bezeichner, Bezeichner Regeln.',
					'lowerCamelCase',
					'UpperCamelCase',
					'SCREAMING_SNAKE_CASE'
				]}
				refLinks={[
					{
						url: 'https://www.w3schools.com/js/js_reserved.asp',
						description: 'Reservierte Bezeichner und mehr'
					}
				]}
			/>
			<Topic
				header='Vordefinierte Funktionen'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Window/alert',
						description: 'alert()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Window/confirm',
						description: 'confirm()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Window/prompt',
						description: 'prompt()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Console',
						description: 'console.log() console.warn() console.error()'
					}
				]}
				refLinks={[
					{
						url: 'https://www.w3schools.com/js/js_functions.asp',
						description: 'Dokumentation über JavaScript Funktionen'
					}
				]}
			/>
			<Topic
				header='Variablen, Funktionen und dynamische Datentypen'
				list={['var, let, const', 'Scope/Gültigkeitsbereiche', 'Multiple Komma separierte Variablen']}
				codeLanguage='html'
				code={codeBlocks['week1day1'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Grammar_and_Types',
						description: 'Mehr Informationen'
					}
				]}
			/>
			<DayJumperButtons WEEK='1' DAY='1' />
		</>
	)
}
