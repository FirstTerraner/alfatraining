import DayJumperButtons from '../../../DayJumper'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek1'
import Topic from '../../../Topic'

export default function Week1Day3() {
	return (
		<>
			<span>Datum: 26.01.2022</span>
			<Topic
				header='Relationale Operatoren'
				codeLanguage='javascript'
				code={codeBlocks['week1day3'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Expressions_and_Operators',
						description: 'Meistere Ausdrücke und Operatoren'
					}
				]}
			/>
			<Topic
				header='Kontrollstrukturen, Bedingungen'
				codeLanguage='javascript'
				code={codeBlocks['week1day3'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/if...else',
						description: 'WENN du mehr erfahren willst, DANN klicke hier drauf'
					}
				]}
			/>
			<Topic
				header='Logische Operatoren'
				codeLanguage='javascript'
				code={codeBlocks['week1day3'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators',
						description: 'Alle Ausdrücke und Operatoren'
					}
				]}
			/>
			<Topic
				header='Arbeiten mit Strings'
				list={[
					'indexOf()',
					'lastIndexOf()',
					'BtoUpperCase()',
					'toLowerCase()',
					'slice()',
					'subString()',
					'trim()',
					'charAt()',
					'includes()',
					'startsWith()',
					'endsWith()',
					'repeat()',
					'padStart()',
					'String() / toString()',
					'split()'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day3'][3]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String',
						description: 'String Dokumentation'
					}
				]}
			/>
			<Topic
				header='Arrays'
				codeLanguage='javascript'
				code={codeBlocks['week1day3'][4]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array',
						description: 'Array Dokumentation'
					}
				]}
			/>
			<DayJumperButtons WEEK='1' DAY='3' />
		</>
	)
}

