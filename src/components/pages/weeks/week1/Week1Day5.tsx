import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek1'

export default function Week1Day5() {
	return (
		<>
			<span>Datum: 28.01.2022</span>
			<Topic
				header='Array Methoden Teil 2'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf',
						description: 'indexOf()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/lastIndexOf',
						description: 'lastIndexOf()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/includes',
						description: 'includes()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/concat',
						description: 'concat()'
					}
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day5'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array',
						description: 'Array Dokumentation'
					}
				]}
			/>
			<Topic
				header='Objekte'
				list={['Eigenschaften (Objektgebundene Variablen / Konstanten)', 'Methoden (Objektgebundene Funktionen)']}
				codeLanguage='javascript'
				code={codeBlocks['week1day5'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Object',
						description: 'Dokumentation über Objekte'
					}
				]}
			/>
			<Topic
				header='Das Date Objekt'
				list={[
					'Date.now()',
					'Date.getDate()',
					'Date.getMonth()',
					'Date.getDay()',
					'Date.getFullYear()',
					'Date.getHours()',
					'Date.getMinutes()',
					'Date.getSeconds()',
					'Date.getMilliseconds()',
					'Date.getTime()',
					'UTC-Weltzeit & UTC-Methoden',
					'Date.toLocaleString()'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day5'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Date',
						description: 'Date Objekt Dokumentation'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Intl#locale_identification_and_negotiation',
						description: 'Mehr Informationen über locale IDs'
					}
				]}
			/>
			<Topic
				header='JavaScript Object Notation JSON'
				list={[
					'JSON.stringify()',
					'JSON.parse()'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day5'][3]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Learn/JavaScript/Objects/JSON',
						description: 'Arbeiten mit JSON'
					}
				]}
			/>
			<Topic
				header='Cookies Teil 1'
				codeLanguage='javascript'
				code={codeBlocks['week1day5'][4]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie',
						description: 'Document.cookie'
					}
				]}
			/>
			<DayJumperButtons WEEK='1' DAY='5' />
		</>
	)
}
