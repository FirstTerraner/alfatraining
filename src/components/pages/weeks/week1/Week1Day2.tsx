import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek1'

export default function Week1Day2() {
	return (
		<>
			<span>Datum: 25.01.2022</span>
			<Topic
				header='Datentypen und dynamische Typisierung'
				list={['Number', 'String (Zeichenkette)', 'Boolean (true oder false)', 'Object', 'Undefined', 'Null', 'Array (vordefiniertes Objekt)']}
				codeLanguage='javascript'
				code={codeBlocks['week1day2'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Data_structures',
						description: 'Lerne mehr über Datentypen und Datenstrukturen'
					}
				]}
			/>
			<Topic
				header='Der typeof Operator'
				codeLanguage='javascript'
				code={codeBlocks['week1day2'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/typeof',
						description: 'Mehr Informationen'
					}
				]}
			/>
			<Topic
				header='Numerische Operatoren'
				list={['Addition (+)', 'Substraktion (-)', 'Division (/)', 'Multiplikation (*)', 'Modulo (%)', 'Potenz (**)']}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operatorss',
						description: 'Alle Ausdrücke und Operatoren'
					}
				]}
			/>
			<Topic
				header='Implizite & explizite Typkonvertierung'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/parseInt',
						description: 'parseInt()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/parseFloat',
						description: 'parseFloat()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Math',
						description: 'Math Objekt'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed',
						description: 'toFixed()'
					}
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day2'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Glossary/Type_Conversion',
						description: 'Mehr über Typkonvertierungen'
					}
				]}
			/>
			<DayJumperButtons WEEK='1' DAY='2' />
		</>
	)
}

