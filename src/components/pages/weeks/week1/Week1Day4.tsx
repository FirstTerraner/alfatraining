import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek1'

export default function Week1Day4() {
	return (
		<>
			<span>Datum: 27.01.2022</span>
			<Topic
				header='Schleifen'
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Loops_and_iteration',
						description: 'Lerne mehr über Schleifen und Iterationen'
					}
				]}
			/>
			<Topic
				header='Break und Continue'
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][1]}
			/>
			<Topic
				header='Eigene Funktionen'
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Functions',
						description: 'Dokumentation'
					}
				]}
			/>
			<Topic
				header='Funktionsausdruck'
				list={[
					'Unterliegt nicht dem Hoisting.'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][3]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/function',
						description: 'Dokumentation'
					}
				]}
			/>
			<Topic
				header='Guards oder Wächter für Funktionen'
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][4]}
			/>
			<Topic
				header='Sofort ausführbarer Funktionsausdruck IIFE'
				list={[
					'Schaffen Scope für var-Variablen.'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][5]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Glossary/IIFE',
						description: 'Mehr Informationen'
					}
				]}
			/>
			<Topic
				header='Array Beispiele'
				codeLanguage='javascript'
				code={codeBlocks['week1day4'][6]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array',
						description: 'Array Dokumentation'
					}
				]}
			/>
			<Topic
				header='Array Methoden Teil 1'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/push',
						description: 'push()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/pop',
						description: 'pop()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift',
						description: 'unshift()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/shift',
						description: 'shift()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/splice',
						description: 'splice()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/sort',
						description: 'sort()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse',
						description: 'reverse()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/join',
						description: 'join()'
					}

				]}
			/>
			<DayJumperButtons WEEK='1' DAY='4' />
		</>
	)
}
