import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek5'

export default function Week5Day5() {
	return (
		<>
			<span>Datum: 25.02.2022</span>
			<Topic
				header='Konstruktorfunktionen'
				list={[
					'Objektaufbau mit pseudoklassischer Objektorientierung',
					'Objekte auf Grundlage einer Konstruktorfunktion'
				]}
				linkList={[
					{
						url: 'https://www.mediaevent.de/javascript/Javascript-Objekte-2.html',
						description: 'Funktionen als Kontruktoren'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Classes/constructor',
						description: 'Der Kontruktor'
					}
				]}
			/>
			<Topic
				header='Die Klassen Syntax'
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Classes',
					description: 'JavaScript Klassen'
				}]}
			/>
			<Topic
				header='Getter und Setter'
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/get',
						description: 'Getter'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/set',
						description: 'Setter'
					}
				]}
			/>
			<Topic
				header='Alternativen zu Getter und Setter'
				list={['Underscore']}
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][2]}
			/>
			<Topic
				header='Klassen erweitern'
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][3]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Classes/extends',
					description: 'Klassen erweitern'
				}]}
			/>
			<Topic
				header='Statische Methoden definieren'
				list={['Kann nur auf der Klassen-Instanz aufgerufen werden', 'Gebräuchliche Konvention in SCREAMING_SNAKE_CASE']}
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][4]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Classes/static',
					description: 'Statische Methoden'
				}]}
			/>
			<Topic
				header='Symbole in Klassen'
				list={['Symbole in Klassen im Zusammenhang mit Settern und Gettern verwenden']}
				codeLanguage='javascript'
				code={codeBlocks['week5day5'][5]}
			/>
			<DayJumperButtons WEEK='5' DAY='5' />
		</>
	)
}