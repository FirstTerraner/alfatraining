import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek5'

export default function Week5Day2() {
	return (
		<>
			<span>Datum: 22.02.2022</span>
			<Topic
				header='Template Strings'
				list={[
					'Werden in Backticks gepackt',
					'Variablen werden in Platzhalter hinzugefügt: `Meine Variable: ${var}`',
					'Leerzeichen und Zeilenumbrüche werden interpretiert'
				]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Template_literals',
					description: 'Template Strings Referenz'
				}]}
			/>
			<Topic
				header='Ternärer Operator'
				list={[
					'Kurze Schreibweise für einfache "if, else" Abfragen',
					'Sollte nicht für "else if" verwendet werden, um die Leserlichkeit beizubehalten'
				]}
				codeLanguage='javascript'
				code='let price = withTaxRate ? 500 : 50'
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/Conditional_Operator',
					description: 'Bedingter (ternärer) Operator'
				}]}
			/>
			<Topic
				header='Reguläre Ausdrücke'
				list={[
					'Wird über den Konstruktor oder die Literalschreibweise erzeugt',
					'Bietet Methoden wie: test(), exec(), ...',
					'String Methoden können mit regulären Ausdrücken arbeiten: toString(), match(), replace(), search()',
					'Flags',
					'Ausdrücke',
					'Definition von Zeichenklassen',
					'Vordefinierte Zeichenklassen',
					'Positionierung / Begrenzung',
					'Quantifizierer',
					'Gruppierungen'
				]}
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test',
						description: 'test()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/exec',
						description: 'exec()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/toString',
						description: 'toString()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/match',
						description: 'match()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/replace',
						description: 'replace()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/String/search',
						description: 'search()'
					}
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day2'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Guide/Regular_Expressions',
						description: 'Dokumentation über reguläre Ausdrücke'
					},
					{
						url: 'https://regex101.com',
						description: 'Teste Regex auf regex101.com'
					}
				]}
			/>
			<DayJumperButtons WEEK='5' DAY='2' />
		</>
	)
}