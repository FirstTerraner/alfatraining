import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek5'

export default function Week5Day4() {
	return (
		<>
			<span>Datum: 24.02.2022</span>
			<Topic
				header='REST-Parameter'
				description='Mit der Rest Parameter Syntax kann man beliebig viele Parameter als Array empfangen.'
				list={['Seit ES6', 'REST-Parameter referenziert ein Array']}
				codeLanguage='javascript'
				code={codeBlocks['week5day4'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/rest_parameters',
					description: 'REST-Parameter Dokumentation'
				}]}
			/>
			<Topic
				header='Events und Event-Objekt'
				description='Das Event Interface repräsentiert jegliches Ereignis, das im DOM auftritt und besteht aus 3 Phasen:	'
				list={[
					'Die absteigende (capturing) Phase',
					'Die angekommene (target) Phase',
					'Die aufsteigende (bubbeling) Phase',
					'preventDefault(), (entspricht return false vom Event-Handler)',
					'stopPropagation()'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day4'][1]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/API/Event',
					description: 'Event Dokumentation'
				}]}
			/>
			<Topic
				header='Prototypen - Vorlagen für Objekte'
				list={['Factory Function', 'Object.create()', 'Ausgelagerte Funktionen im Prototyp', 'Setzen / Zuordnen eines Prototypen']}
				codeLanguage='javascript'
				code={codeBlocks['week5day4'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Learn/JavaScript/Objects/Object_prototypes',
						description: 'Object prototypes'
					}
				]}
			/>
			<Topic
				header='Eigenschaftsattribute'
				list={['value', 'writeable', 'enumerable', 'configurable', 'set', 'get']}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Object/create',
						description: 'Object.create() Referenz'
					}
				]}
			/>
			<Topic
				header='Zugriff auf Methoden des Prototypen'
				list={['getPrototypeOf()', 'call()']}
			/>
			<DayJumperButtons WEEK='5' DAY='4' />
		</>
	)
}