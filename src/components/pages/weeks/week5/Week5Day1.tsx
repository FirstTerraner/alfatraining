import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek5'

export default function Week5Day1() {
	return (
		<>
			<span>Datum: 21.02.2022</span>
			<Topic
				header='Weiterführung durch JavaScript Funktionen'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/setTimeout',
						description: 'setTimeout()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/clearTimeout',
						description: 'clearTimeout()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/click',
						description: 'click()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/focus',
						description: 'focus()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/reset',
						description: 'reset()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/setInterval',
						description: 'setInterval()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/clearInterval',
						description: 'clearInterval()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/window/cancelAnimationFrame',
						description: 'requestAnimationFrame()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame',
						description: 'cancelAnimationFrame()'
					}
				]}
			/>
			<Topic
				codeLanguage='html'
				code={codeBlocks['week5day1'][0]}
			/>
			<Topic
				codeLanguage='javascript'
				code={codeBlocks['week5day1'][1]}
			/>
			<Topic
				header='Das Location Objekt und seine Eigenschaften'
				list={['window.location || location', 'Bestandteile des Location Objektes mit console.log(location)']}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Location',
						description: 'Das Location Interface'
					}
				]}
			/>
			<Topic
				header='Objekte und Arrays'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/for...in',
						description: 'Die "for-in" Schleife'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Statements/for...of',
						description: 'Die "for-of" Schleife'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze',
						description: 'Die Objekt-Methode "freeze"'
					}
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day1'][2]}
			/>
			<Topic
				header='Das Form-Data-Objekt'
				linkList={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/append',
						description: 'append()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/set',
						description: 'set()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/has',
						description: 'has()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/delete',
						description: 'delete()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/keys',
						description: 'keys()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/values',
						description: 'values()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/entries',
						description: 'entries()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/get',
						description: 'get()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData/getAll',
						description: 'getAll()'
					},
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day1'][3]}
				refLinks={[{
					url: 'https://developer.mozilla.org/en-US/docs/Web/API/FormData',
					description: 'Die Form-Data Dokumentation'
				}]}
			/>
			<Topic
				header='Der Datentyp "Symbol"'
				list={[
					'Neuer primitiver Datentyp in ES6',
					'Dient lediglich der Beschreibung, nicht Identifikation',
					'WIrd erzeugt über Symbol()',
					'Symbole sind einzigartig, 2 Symbole gleichen sich niemals'
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day1'][4]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol',
						description: 'Symbol Referenz'
					}
				]}
			/>

			<DayJumperButtons WEEK='5' DAY='1' />
		</>
	)
}