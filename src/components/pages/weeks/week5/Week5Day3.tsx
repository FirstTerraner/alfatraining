import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek5'

export default function Week5Day3() {
	return (
		<>
			<span>Datum: 23.02.2022</span>
			<Topic
				header='Pfeilfunktionen'
				list={['Syntax', 'Implizites Return', 'Klammer-Regel für Argumente', 'Das Schlüsselwort "this"']}
				codeLanguage='javascript'
				code={codeBlocks['week5day3'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Functions/Arrow_functions',
					description: 'Dokumentation'
				}]}
			/>
			<Topic
				header='Higher Order Functions'
				description='"Higher Order" Funktionen sind Funktionen, die in anderen Funktionen operieren, indem diese als Argumente übergeben werden oder zurückgegeben werden.'
				list={['Funktionsschreibweisen', 'Callbacks für Array Methoden']}
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/sort',
						description: 'sort()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach',
						description: 'forEach()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/map',
						description: 'map()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/filter',
						description: 'filter()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce',
						description: 'reduce()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/every',
						description: 'every()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/some',
						description: 'some()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/find',
						description: 'find()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex',
						description: 'findIndex()'
					},
				]}
				codeLanguage='javascript'
				code={codeBlocks['week5day3'][1]}
			/>
			<Topic
				header='Die Spread Syntax'
				codeLanguage='javascript'
				code={codeBlocks['week5day3'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Operators/Spread_syntax',
						description: 'Die Spread Syntax'
					}
				]}

			/>
			<DayJumperButtons WEEK='5' DAY='3' />
		</>
	)
}