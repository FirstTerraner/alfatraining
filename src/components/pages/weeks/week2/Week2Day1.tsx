import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek2'

export default function Week2Day1() {
	return (
		<>
			<span>Datum: 31.01.2022</span>
			<Topic
				header='Cookies Teil 2'
				list={['Cookie Hinweis für den Nutzer', 'Get & Set Cookie']}
				codeLanguage='javascript'
				code={codeBlocks['week2day1'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie',
						description: 'Document.cookie'
					}
				]}
			/>
			<Topic
				header='Web Storage'
				list={['Local Storage', 'Session Storage']}
				codeLanguage='javascript'
				code={codeBlocks['week2day1'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Web_Storage_API',
						description: 'Erkunde die Web Storage'
					}
				]}
			/>
			<Topic
				header='Das window-Objekt'
				list={['innerWidth', 'innerHeight', 'outerHeight', 'outerWidth', 'scrollX', 'scrollY', 'print()']}
				codeLanguage='javascript'
				code={codeBlocks['week2day1'][2]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Window',
						description: 'Window Referenz'
					}
				]}
			/>
			<Topic
				header='Document Object Model'
				list={['document.title', 'document.cookie', 'document.URL', 'document.domain', 'document.lastModified', 'document.body', 'getElementById()', 'getElementsByClassName()']}
				codeLanguage='javascript'
				code={codeBlocks['week2day1'][3]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Document_Object_Model',
						description: 'Mehr Informationen über den DOM'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Document/querySelector',
						description: 'Der querySelector()'
					}
				]}
			/>
			<Topic
				header='Zugriff auf den Inhalt von HTML-Elementen'
				list={[
					'innerHTML',
					'innerText',
					'textContent',
					'className',
					'classList',
					'classList.replace()',
					'classList.toggle()',
					'classList.contains()',
					'getAttribute()',
					'setAttribute()',
					'hasAttribute()'
				]}
				html={`<h1></h1>
<p></p>
<ul id="secondList">
    <li>item 1</li>
    <li>item 2</li>
</ul>`}
				codeLanguage='javascript'
				code={codeBlocks['week2day1'][4]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Manipulating_documents',
						description: 'DOM Manipulation'
					}
				]}
			/>
			<DayJumperButtons WEEK='2' DAY='1' />
		</>
	)
}
