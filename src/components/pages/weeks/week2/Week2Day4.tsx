import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek2'
import ImageSection from '../../../ImageSection'

export default function Week2Day4() {
	return (
		<>
			<span>Datum: 03.02.2022</span>
			<Topic
				header='Element Knoten erzeugen und einfügen'
				linkList={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Document/createElement',
						description: 'createElement()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Node/appendChild',
						description: 'appendChild()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Node/cloneNode',
						description: 'cloneNode()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/remove',
						description: 'remove()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Node/removeChild',
						description: 'removeChild()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Node/replaceChild',
						description: 'replaceChild()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/replaceWith',
						description: 'replaceWith()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore',
						description: 'insertBefore()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentElement',
						description: 'insertAdjacentElement()'
					},
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Element/insertAdjacentHTML',
						description: 'insertAdjacentHTML()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentText',
						description: 'insertAdjacentText()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/prepend',
						description: 'prepend()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/append',
						description: 'append()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/before',
						description: 'before()'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/Element/after',
						description: 'after()'
					}
				]}
				codeLanguage='javascript'
				code={codeBlocks['week2day4'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/API/Element#methods',
					description: 'Referenz der Methoden für DOM-Elemente'
				}]}
			/>
			<Topic
				header='DOM-Traversal'
			/>
			<ImageSection
				imgSrc='/dom_traversal.png'
				width={700}
				height={350}
				imgReference='https://en.owl.institute/javascript-fundamentals/javascript-manipulating-html-with-ease/going-on-a-family-trip-siblings-insertbefore-summary'
				referenceTxt={['Source:', 'Open Web Learning']}
			/>
			<Topic
				header='Polyfill für ältere Browser'
				description='Polyfill ist ein üblicherweise in JavaScript geschriebener Code-Baustein, der dazu dient, moderne HTML-, CSS- oder JavaScript-Funktionalitäten in älteren Browsern zur Verfügung zu stellen, die diese Funktionailtät nicht von Haus aus unterstützen.'
				codeLanguage='javascript'
				code={codeBlocks['week2day4'][1]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Glossary/Polyfill',
						description: 'Polyfill Dokumentation'
					},
					{
						url: 'https://polyfill.io/v3/',
						description: 'polyfill.io'
					}
				]}
			/>

			<DayJumperButtons WEEK='2' DAY='4' />
		</>
	)
}
