import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek2'
import ImageSection from '../../../ImageSection'

export default function Week2Day3() {
	return (
		<>
			<span>Datum: 02.02.2022</span>
			<Topic
				header='Das Style Objekt'
				list={['CSS Eigenschaften ändern', 'Read-only', 'getComputedStyle()']}
				refLinks={[{
					url: 'https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement/style',
					description: 'HTMLElement.style'
				}]}
			/>
			<Topic
				header='Übungsaufgabe 1'
				description='Implementiere die passenden Funktionen, um das "eBook-Beispiel" bedienbar zu machen.'
				list={['Probiere es selbst:']}
				html={codeBlocks['week2day3'][0]}
				css={codeBlocks['week2day3'][1]}
				codeLanguage='javascript'
				code={codeBlocks['week2day3'][2]}
			/>
			<Topic
				header='Übungsaufgabe 2'
				description='Füge jedem Button eine Funktionalität zu, die die Schriftgröße des Paragrafs ändert.'
				list={['Probiere es selbst:']}
				html={codeBlocks['week2day3'][3]}
				codeLanguage='javascript'
				code={codeBlocks['week2day3'][4]}
			/>
			<Topic
				header='Übungsaufgabe 3'
				description='Hier wird die Schriftgröße des Paragrafs durch 2 Buttons bestimmt: "+" und "-". Vergrößere oder verkleinere die Schriftgröße um 5px, je nach Benutzereingabe.'
				list={['Probiere es selbst:']}
				html={codeBlocks['week2day3'][5]}
				codeLanguage='javascript'
				code={codeBlocks['week2day3'][6]}
			/>
			<ImageSection
				imgSrc='/monkeyuser_issues.jpg'
				width={500}
				height={500}
				imgReference='https://twitter.com/ismonkeyuser'
				referenceTxt={['Illustriert von:', 'Monkeyuser']}
			/>
			<DayJumperButtons WEEK='2' DAY='3' />
		</>
	)
}
