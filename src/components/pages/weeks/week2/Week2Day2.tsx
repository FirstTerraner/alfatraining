import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek2'

export default function Week2Day2() {
	return (
		<>
			<span>Datum: 01.02.2022</span>
			<Topic
				header='Zugriff über IDL-Attribute'
				html='<progress value="1" min="0" max="10"></progress>'
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][0]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Glossary/IDL',
						description: 'Content vs. IDL Attribute'
					}
				]}
			/>
			<Topic
				header='Boolsche Attribute'
				list={['checked', 'required', 'selected', 'disabled', 'async', 'defer']}
				html='<button>Button-Text</button>'
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][1]}
			/>
			<Topic
				header='Unterschiede zwischen Content- und IDL-Attribute'
				html='<input id="eingabe"></input>'
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][2]}
			/>
			<Topic
				header='Event Beispiele'
				list={[
					'click',
					'dblclick',
					'mouseenter',
					'mouseleave',
					'mousemove',
					'mousedown',
					'mouseup',
					'keyup',
					'keydown',
					'keypress (veraltet)',
					'submit',
					'reset',
					'change',
					'input',
					'focus',
					'blur',
					'select',
					'scroll'
				]}
				html='<input id="eingabe"></input>'
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][3]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/Events',
						description: 'Event Referenz'
					}
				]}
			/>
			<Topic
				header='DOM Event Handler'
				list={['load', 'addEventlistener', 'removeEventListener', 'DOMContentLoaded']}
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][4]}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener',
						description: 'addEventListener'
					}
				]}
			/>
			<Topic
				header='Übungsaufgaben'
				list={['Probiere es selbst:']}
				html={codeBlocks['week2day2'][5]}
				codeLanguage='javascript'
				code={codeBlocks['week2day2'][6]}
			/>
			<DayJumperButtons WEEK='2' DAY='2' />
		</>
	)
}
