import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek2'
import ImageSection from '../../../ImageSection'

export default function Week2Day5() {
	return (
		<>
			<span>Datum: 04.02.2022</span>
			<Topic
				header='Übungsaufgabe: Einkaufszettel'
				description='Erstelle eine Einkaufsliste, die vom Benutzer bearbeitet werden kann.'
				list={['Probiere es selbst: ']}
				html={codeBlocks['week2day5'][0]}
				css={codeBlocks['week2day5'][1]}
				codeLanguage='javascript'
				code={codeBlocks['week2day5'][2]}
			/>
			<Topic
				header='Fehlerbehandlung'
				list={['Syntaxfehler', 'Laufzeitfehler', 'Logische Fehler / Bugs', 'Debugging', 'Try-Catch']}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/JavaScript/Reference/Errors',
					description: 'JavaScript Fehler Refenrenz'
				}]}
			/>
			<Topic
				header='Theoretische Einführung in AJAX'
				refLinks={[{
					url: 'https://www.w3schools.com/js/js_ajax_intro.asp',
					description: 'Einführung in AJAX'
				}]}
			/>
			<ImageSection
				imgSrc='/ajax-traversal.jpg'
				width={700}
				height={350}
				imgReference='https://www.w3schools.com/js/js_ajax_intro.asp'
				referenceTxt={['Source:', 'W3schools']}
			/>
			<DayJumperButtons WEEK='2' DAY='5' />
		</>
	)
}
