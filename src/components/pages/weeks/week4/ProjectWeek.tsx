import { Button, Stack } from '@mui/material'
import Link from 'next/link'
import styles from '../../../../../styles/Home.module.css'
import DoneIcon from '@mui/icons-material/Done'
import ImageSection from '../../../ImageSection'
import { themes } from '../../../../../styles/colors'
import Topic from '../../../Topic'

export default function ProjectWeek() {
	return (
		<div className={styles.noContent}>

			<h2>
				Projektwoche
			</h2>

			<p className={styles.centered}>
				Klicke auf dem Button um den Quellcode des Projektes auf GitLab zu sehen oder sieh dir die&nbsp;
				<Link href='https://gitlab.com/mkemal/do-it/-/blob/main/README.md'>
					<a target='_blank' rel='noopener noreferrer' style={{ color: themes.muiBlue }}>
						README.md
					</a>
				</Link>
				&nbsp;Datei an.
			</p>

			<p>
				<Link href='https://gitlab.com/mkemal/do-it'>
					<a target='_blank' rel='noopener noreferrer'>
						<Button variant='outlined' color='primary' endIcon={<DoneIcon />}>
							Do it Liste
						</Button>
					</a>
				</Link>
			</p>
			<Topic header='Screenshots' />
			<Stack direction='column' sx={{ margin: '0 0 2em 0' }}>
				<ImageSection
					imgSrc='/mainPage.png'
					width={900}
					height={420}
					imgReference=''
					referenceTxt={['Screenshot der Hauptseite']}
				/>
				<div style={{ margin: '1em 0' }}></div>
				<ImageSection
					imgSrc='/loginPage.png'
					width={900}
					height={420}
					imgReference=''
					referenceTxt={['Screenshot einer Beispiel-Anmeldung']}
				/>
				<div style={{ margin: '1em 0' }}></div>
				<ImageSection
					imgSrc='/todoPage.png'
					width={900}
					height={420}
					imgReference=''
					referenceTxt={['Screenshot einer Beispiel To-Do Liste']}
				/>
			</Stack>
		</div>
	)
}