import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek3'

export default function Week3Day3() {
	return (
		<>
			<span>Datum: 09.02.2022</span>
			<Topic
				header='JSON POST mit XMLHttpRequest'
				refLinks={[{
					url: 'https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/send',
					description: 'GET und POST Beispiele'
				}]}
			/>
			<Topic
				header='Theoretische Einführung in JQuery'
				list={['Selektoren', 'Konfliktloser Modus', 'ready()', 'Zugriff auf Attribute', '.html()', '.first()', 'Ältere jQuery Versionen']}
				codeLanguage='javascript'
				code={codeBlocks['week3day3'][0]}
				refLinks={[
					{
						url: 'https://jquery.com/',
						description: 'JQuery Homepage'
					}
				]}
			/>
			<DayJumperButtons WEEK='3' DAY='3' />
		</>
	)
}