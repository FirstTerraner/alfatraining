import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'

export default function Week3Day2() {
	return (
		<>
			<span>Datum: 08.02.2022</span>
			<Topic header='HTML-Seiten mit AJAX laden' />
			<Topic
				header='History-Object'
				list={['history.back()', 'history.forward()', 'history.go()', 'history.length', 'history.pushState()']}
				refLinks={[
					{
						url: 'https://developer.mozilla.org/de/docs/Web/API/Window/history',
						description: 'Window.history'
					},
					{
						url: 'https://developer.mozilla.org/en-US/docs/Web/API/History_API',
						description: 'History API'
					}
				]}
			/>
			<Topic header='Übungsaufgaben mit AJAX' />
			<DayJumperButtons WEEK='3' DAY='2' />
		</>
	)
}
