import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek3'

export default function Week3Day1() {
	return (
		<>
			<span>Datum: 07.02.2022</span>
			<Topic
				header='XML Dateistruktur'
				list={['DOM Parser', 'XMLSerializer', 'Daten auffangen - auswerten - verwenden/einbauen', 'XMLHttpRequest', 'open()', 'send()']}
				codeLanguage='javascript'
				code={codeBlocks['week3day1'][0]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/XML/XML_introduction',
					description: 'XML Einführung'
				}]}
			/>
			<Topic
				header='XMLHttpRequest'
				codeLanguage='javascript'
				code={codeBlocks['week3day1'][1]}
				refLinks={[{
					url: 'https://developer.mozilla.org/de/docs/Web/API/XMLHttpRequest',
					description: 'XMLHttpRequest Dokumentation'
				}]}
			/>
			<Topic
				header='Übungsaufgaben mit XML Daten'
			/>
			<DayJumperButtons WEEK='3' DAY='1' />
		</>
	)
}
