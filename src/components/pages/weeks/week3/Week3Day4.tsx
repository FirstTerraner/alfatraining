import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'
import { codeBlocks } from '../codeBlocks/codeBlocksWeek3'

export default function Week3Day4() {
	return (
		<>
			<span>Datum: 10.02.2022</span>
			<Topic
				header='Arbeiten mit jQuery'
				list={['Content erzeugen', 'Styles setzen', 'Klassen', 'Events', 'Vordefinierte Effekte', 'Eigene Effekte estellen', 'Navigation im DOM']}
				codeLanguage='javascript'
				code={codeBlocks['week3day4'][0]}
				refLinks={[{
					url: 'https://api.jquery.com/',
					description: 'JQuery API Dokumentation'
				}]}
			/>
			<Topic header='Übungsaufgaben mit jQuery' description='Erstelle einen Image-Slider mit Hilfe von jQuery' />
			<DayJumperButtons WEEK='3' DAY='4' />
		</>
	)
}