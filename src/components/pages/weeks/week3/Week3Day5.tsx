import DayJumperButtons from '../../../DayJumper'
import Topic from '../../../Topic'

export default function Week3Day5() {
	return (
		<>
			<span>Datum: 11.02.2022</span>
			<Topic
				header='Arbeiten mit jQuery'
				list={['Content erzeugen', 'Styles setzen', 'Klassen', 'Events', 'Vordefinierte Effekte', 'Eigene Effekte estellen', 'Navigation im DOM']}
				refLinks={[{
					url: 'https://api.jquery.com/',
					description: 'JQuery API Dokumentation'
				}]}
			/>
			<Topic header='Vorbereitung auf die Projekt-Woche' />
			<DayJumperButtons WEEK='3' DAY='5' />
		</>
	)
}