import styles from '../../../styles/DayPage.module.css'
import Metatags from '../Metatags'
import Navigation from '../navigation/Navigation'
import Footer from '../Footer'
import { useRouter } from 'next/router'
import DayJumperButtons from '../DayJumper'
import { isBadPath } from '../../helper'
import BadPage from './BadPage'
import ScrollTopButton from '../ScrollTopBtn'
import StyleTag from '../../../styles/styleTag'
import CookieBanner from '../CookieBanner'
import Week1Day1 from './weeks/week1/Week1Day1'
import Week1Day2 from './weeks/week1/Week1Day2'
import Week1Day3 from './weeks/week1/Week1Day3'
import Week1Day4 from './weeks/week1/Week1Day4'
import Week1Day5 from './weeks/week1/Week1Day5'
import Week2Day1 from './weeks/week2/Week2Day1'
import Week2Day2 from './weeks/week2/Week2Day2'
import Week2Day3 from './weeks/week2/Week2Day3'
import Week2Day4 from './weeks/week2/Week2Day4'
import Week2Day5 from './weeks/week2/Week2Day5'
import Week3Day1 from './weeks/week3/Week3Day1'
import Week3Day2 from './weeks/week3/Week3Day2'
import Week3Day3 from './weeks/week3/Week3Day3'
import Week3Day4 from './weeks/week3/Week3Day4'
import Week3Day5 from './weeks/week3/Week3Day5'
import ProjectWeek from './weeks/week4/ProjectWeek'
import Week5Day1 from './weeks/week5/Week5Day1'
import Week5Day2 from './weeks/week5/Week5Day2'
import Week5Day3 from './weeks/week5/Week5Day3'
import Week5Day4 from './weeks/week5/Week5Day4'
import Week5Day5 from './weeks/week5/Week5Day5'
import Week6Day1 from './weeks/week6/Week6Day1'
import Week6Day2 from './weeks/week6/Week6Day2'
import Week6Day3 from './weeks/week6/Week6Day3'
import Week6Day4 from './weeks/week6/Week6Day4'

export default function DayPage() {
	const router = useRouter()
	const WEEK = router.query.week?.toString()
	const DAY = router.query.day?.toString()
	return (
		<>
			<StyleTag />
			<Metatags
				title={`Alfaprojekt${WEEK && DAY ? ` W.${WEEK} - T.${DAY}` : ''}`}
				description='22 Wochen Web-Entwicklung mit Alfatraining.'
			/>
			{/* hide navigation weeks if the path is not valid */}
			{isBadPath(WEEK, DAY) ? <Navigation /> : <Navigation WEEK={WEEK} DAY={DAY} />}
			<div className={styles.container}>
				<main className={styles.main}>
					{/* page jumper */}
					<DayJumperButtons WEEK={WEEK} DAY={DAY} />
					{/* page content */}
					{getCorrectDayComponent(WEEK, DAY)}
				</main>
			</div>
			<ScrollTopButton WEEK={WEEK} DAY={DAY} />
			<Footer />
			<CookieBanner />
		</>
	)
}

const getCorrectDayComponent = (WEEK?: string, DAY?: string) => {
	// guard
	if (!WEEK || !DAY) { return <BadPage /> }
	if (isBadPath(WEEK, DAY)) { return <BadPage /> }
	// Project weeks
	if (WEEK == '4') { return <ProjectWeek /> }
	// return wished page
	switch (`${WEEK}-${DAY}`) {
		case '1-1': return <Week1Day1 />
		case '1-2': return <Week1Day2 />
		case '1-3': return <Week1Day3 />
		case '1-4': return <Week1Day4 />
		case '1-5': return <Week1Day5 />
		case '2-1': return <Week2Day1 />
		case '2-2': return <Week2Day2 />
		case '2-3': return <Week2Day3 />
		case '2-4': return <Week2Day4 />
		case '2-5': return <Week2Day5 />
		case '3-1': return <Week3Day1 />
		case '3-2': return <Week3Day2 />
		case '3-3': return <Week3Day3 />
		case '3-4': return <Week3Day4 />
		case '3-5': return <Week3Day5 />
		case '5-1': return <Week5Day1 />
		case '5-2': return <Week5Day2 />
		case '5-3': return <Week5Day3 />
		case '5-4': return <Week5Day4 />
		case '5-5': return <Week5Day5 />
		case '6-1': return <Week6Day1 />
		case '6-2': return <Week6Day2 />
		case '6-3': return <Week6Day3 />
		case '6-4': return <Week6Day4 />
		default: return <BadPage noContent />
	}
}
