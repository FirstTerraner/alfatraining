import Link from 'next/link'
import { useRouter } from 'next/router'
import Highlight from 'react-highlight'
import styles from '../../styles/DayPage.module.css'
import { handleTitleClick } from '../helper'
import homeStyles from '../../styles/Home.module.css'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import ContentCopyIcon from '@mui/icons-material/ContentCopy'
import { ITopicProps } from '../models'
import { IconButton, Stack, Tooltip } from '@mui/material'
import Image from 'next/image'
import React from 'react'

export default function Topic({ header, description, list, linkList, html, css, code, codeLanguage, refLinks }: ITopicProps) {

	const router = useRouter()

	const [isCodeCopied, setIsCodeCopied] = React.useState(false)

	const copyToClipboard = (txt: string) => {
		navigator.clipboard.writeText(txt).then(() => setIsCodeCopied(true), () => setIsCodeCopied(false))
	}

	React.useEffect(() => {
		if (!isCodeCopied) { return }
		// console.log('timeout started')
		const copiedTimeout = setTimeout(() => {
			setIsCodeCopied(false)
			clearTimeout(copiedTimeout)
		}, 2000)
	}, [isCodeCopied])

	return (
		<div className={styles.topicWrapper}>
			{header &&
				<h1
					id={header.toLowerCase().replaceAll(' ', '-')}
					onClick={e => handleTitleClick(e, router)}
					className='pointer fitContent'
				>
					{header}
				</h1>
			}
			{description && <p>{description}</p>}
			{list &&
				<ul>
					{list.map(string => <li key={string}>{string}</li>)}
				</ul>
			}
			{linkList &&
				<Stack direction='column' spacing={1} sx={{ margin: '0 0 1em 2.5em' }}>
					{linkList.map(linkObj => (
						<Link key={linkObj.url} href={linkObj.url}>
							<a target='_blank' rel='noopener noreferrer' className={homeStyles.flexLinkList}>
								{linkObj.description} <ArrowRightIcon fontSize='small' sx={{ position: 'absolute' }} />
							</a>
						</Link>
					))}
				</Stack>
			}
			{html &&
				<>
					<div className={styles.codeHeader}>
						<div className={styles.headerTxtWrap}>
							<Image
								src={'/html-logo.png'}
								alt=''
								width={35}
								height={35}
								quality={100}
								placeholder='empty'
								loading='lazy'
							/>
							<span className={styles.headerTxt}>HTML</span>
						</div>
						<Tooltip title={isCodeCopied ? 'Kopiert!' : 'Code kopieren'}>
							<IconButton
								sx={{ color: '#f8f8f2' }}
								onClick={() => copyToClipboard(html)}
							>
								<ContentCopyIcon />
							</IconButton>
						</Tooltip>
					</div>
					<Highlight className='html'>
						{html}
					</Highlight>
				</>
			}
			{css &&
				<>
					<div className={styles.codeHeader}>
						<div className={styles.headerTxtWrap}>
							<Image
								src={'/css-logo.png'}
								alt=''
								width={35}
								height={35}
								quality={100}
								placeholder='empty'
								loading='lazy'
							/>
							<span className={styles.headerTxt}>CSS</span>
						</div>
						<Tooltip title={isCodeCopied ? 'Kopiert!' : 'Code kopieren'}>
							<IconButton
								sx={{ color: '#f8f8f2' }}
								onClick={() => copyToClipboard(css)}
							>
								<ContentCopyIcon />
							</IconButton>
						</Tooltip>
					</div>
					<Highlight className='css'>
						{css}
					</Highlight>
				</>
			}
			{code && codeLanguage &&
				<>
					<div className={styles.codeHeader}>
						<div className={styles.headerTxtWrap}>
							<Image
								src={`/${codeLanguage}-logo.png`}
								alt=''
								width={35}
								height={35}
								quality={100}
								placeholder='empty'
								loading='lazy'
							/>
							<span className={styles.headerTxt}>{formatCodeLanguage(codeLanguage)}</span>
						</div>
						<Tooltip title={isCodeCopied ? 'Kopiert!' : 'Code kopieren'}>
							<IconButton
								sx={{ color: '#f8f8f2' }}
								onClick={() => copyToClipboard(code)}
							>
								<ContentCopyIcon />
							</IconButton>
						</Tooltip>
					</div>
					<Highlight className={codeLanguage}>
						{code}
					</Highlight>
				</>
			}
			{refLinks &&
				<Stack direction='column' spacing={1}>
					{refLinks.map(linkObj => (
						<Link key={linkObj.url} href={linkObj.url}>
							<a target='_blank' rel='noopener noreferrer' className={homeStyles.flexLink}>
								{linkObj.description} <ArrowRightIcon fontSize='small' sx={{ position: 'absolute' }} />
							</a>
						</Link>
					))}
				</Stack>
			}
		</div>
	)
}

const formatCodeLanguage = (language: string) => {
	if (language == 'javascript') { return 'JS' }
	return language.toUpperCase()
}