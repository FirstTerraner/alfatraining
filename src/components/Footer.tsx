import styles from '../../styles/Home.module.css'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import { Stack } from '@mui/material'
import Link from 'next/link'
import { usePrefContext } from '../context/preferences'

export default function Footer() {
	const { isDark } = usePrefContext()
	return (
		<footer className={`${styles.footer} ${isDark ? styles.darkTopBorder : styles.lightTopBorder}`}>
			<Stack direction='row' spacing={3}>
				<p className={styles.subTitle}>
					Open Source
				</p>
				<FavoriteBorderIcon fontSize='small' color='error' />
				<Link href='https://gitlab.com/FirstTerraner/alfatraining/-/blob/main/LICENSE'>
					<a>
						MIT License
					</a>
				</Link>
			</Stack>
		</footer>
	)
}