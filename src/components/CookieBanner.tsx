import { Button } from '@mui/material'
import { themes } from '../../styles/colors'
import { usePrefContext } from '../context/preferences'


export default function CookieBanner() {

	const { isDark, cookieAllowed, toggleCookie, cookieMsgHidden, hideCookieMsg } = usePrefContext()

	if (cookieAllowed || cookieMsgHidden) { return null }

	return (
		<div
			className='cookieBannerWrap'
			style={{
				backgroundColor: isDark ? themes.dark.background : themes.light.background,
				color: isDark ? themes.dark.color : themes.light.color,
				borderTop: `1px solid ${isDark ? themes.dark.border : themes.light.border}`
			}}
		>
			<p className='cookieNote'>Diese Seite verwendet Cookies um Layout-Vorlieben zu speichern.</p>
			<div className='cookieBtnWrap'>
				<Button
					variant='contained'
					color='success'
					onClick={() => {
						hideCookieMsg()
						toggleCookie()
					}}
					sx={{ marginRight: '1em' }}
				>
					OK!
				</Button>
				<span className='underline pointer' onClick={hideCookieMsg}>Nein, danke.</span>
			</div>
		</div>
	)
}