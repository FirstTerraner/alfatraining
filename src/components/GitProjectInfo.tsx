import Link from 'next/link'
import { IGitProject } from '../models'
import ForkLeftIcon from '@mui/icons-material/ForkLeft'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import BugReportIcon from '@mui/icons-material/BugReport'
import UpgradeIcon from '@mui/icons-material/Upgrade'

export default function GitProjectInfo({ gitProject }: { gitProject: IGitProject | null }) {

	if (!gitProject) { return null }

	return (
		<div className='gitProjectWrap'>
			<div className='gitColumnNoHover defaultCursor'>
				<span className='flex'>
					<UpgradeIcon sx={{ marginRight: '5px' }} />
					Aktualisiert
				</span>
				<span>
					{new Date(gitProject.last_activity_at.split('T')[0]).toLocaleDateString()}
				</span>
			</div>
			<Link href='https://gitlab.com/FirstTerraner/alfatraining/-/issues'>
				<a target='_blank' rel='noopener noreferrer'>
					<div className='gitColumn'>
						<span className='flex'>
							<BugReportIcon sx={{ marginRight: '5px' }} />
							Issues
						</span>
						<span className='flex'>
							{gitProject.open_issues_count}
						</span>
					</div>
				</a>
			</Link>
			<Link href='https://gitlab.com/FirstTerraner/alfatraining'>
				<a target='_blank' rel='noopener noreferrer'>
					<div className='gitColumn'>
						<span className='flex'>
							<StarBorderIcon sx={{ marginRight: '5px' }} />
							Stars
						</span>
						<span className='flex'>
							{gitProject.star_count}
						</span>
					</div>
				</a>
			</Link>
			<Link href='https://gitlab.com/FirstTerraner/alfatraining'>
				<a target='_blank' rel='noopener noreferrer'>
					<div className='gitColumn'>
						<span className='flex'>
							<ForkLeftIcon sx={{ marginRight: '5px' }} />
							Forks
						</span>
						<span className='flex'>
							{gitProject.forks_count}
						</span>
					</div>
				</a>
			</Link>
		</div>
	)
}