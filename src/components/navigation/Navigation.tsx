import * as React from 'react'
import Drawer from '@mui/material/Drawer'
import { usePrefContext } from '../../context/preferences'
import NavBar from './NavBar'
import NavList from './NavList'
import { themes } from '../../../styles/colors'

export default function Navigation({ WEEK, DAY }: { WEEK?: string, DAY?: string }) {

	// app theme
	const { isDark } = usePrefContext()

	// navigation state
	const [isNavOpen, setIsNavOpen] = React.useState(false)

	const toggleNav = React.useCallback(() => { setIsNavOpen(prev => !prev) }, [setIsNavOpen])

	const closeNav = React.useCallback(() => { setIsNavOpen(false) }, [setIsNavOpen])

	return (
		<React.Fragment>
			{/* navigation bar */}
			<NavBar WEEK={WEEK} DAY={DAY} toggleNav={toggleNav} />
			{/* slider */}
			<Drawer
				anchor='left'
				open={isNavOpen}
				onClose={closeNav}
				PaperProps={{
					sx: {
						backgroundColor: isDark ? themes.dark.background : themes.light.background,
						color: isDark ? themes.dark.color : themes.light.color
					}
				}}
			>
				<NavList toggleNav={toggleNav} closeNav={closeNav} />
			</Drawer>
		</React.Fragment>
	)
}