import { Box, List, ListItem, ListItemIcon, ListItemText, Divider, ListSubheader, Collapse, ListItemButton } from '@mui/material'
import Link from 'next/link'
import { NextRouter, useRouter } from 'next/router'
import React from 'react'
import { usePrefContext } from '../../context/preferences'
import { TWeekArrayState } from '../../models'
import HomeIcon from '@mui/icons-material/Home'
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import ArrowRightIcon from '@mui/icons-material/ArrowRight'
import { themes } from '../../../styles/colors'
import styles from '../../../styles/Nav.module.css'

export default function NavList({ toggleNav, closeNav }: { toggleNav: () => void, closeNav: () => void }) {

	const router = useRouter()

	const { isDark } = usePrefContext()

	// navList collapsed
	const [isWeekOpen, setIsWeekOpen] = React.useState<TWeekArrayState | undefined>()

	const handleItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		if (!isWeekOpen) { return }
		if (event.currentTarget.id === 'Home') {
			void router.push('/')
			closeNav()
			return
		}
		// close previous items
		for (const [key, value] of Object.entries(isWeekOpen)) {
			if (value) { isWeekOpen[key] = false }
		}
		// toggle
		setIsWeekOpen({ ...isWeekOpen, [event.currentTarget.id]: !isWeekOpen[event.currentTarget.nodeName] })
	}

	// fill state [isWeekOpen] for collapsed nav items
	React.useEffect(() => {
		const isCollapsed: TWeekArrayState = {}
		const weeks = 22
		const currentWeek = router.query.week?.toString()
		let key: string
		for (let i = 0; i < weeks; i++) {
			key = `Woche ${i + 1}`
			// open current week
			if (currentWeek && +currentWeek === i + 1) {
				isCollapsed[key] = true
				continue
			}
			isCollapsed[key] = false
		}
		setIsWeekOpen(isCollapsed)
	}, [router.query.week])

	return (
		<Box className={styles.navListWrap} role='presentation'>
			{/* nav main list */}
			<List>
				{mainList.map(entry => (
					<ListItem key={entry} id={entry} button onClick={handleItemClick}>
						<ListItemIcon>
							<HomeIcon className={styles.navListIcon} sx={{ color: isDark ? themes.dark.color : themes.light.color }} fontSize='small' />
						</ListItemIcon>
						<ListItemText primary={entry} />
					</ListItem>
				))}
			</List>
			<Divider />
			{/* nav week list */}
			<List
				subheader={
					<ListSubheader
						component='div'
						id='nested-list-subheader'
						sx={{
							backgroundColor: isDark ? themes.dark.background : themes.light.background,
							color: isDark ? themes.dark.color : themes.light.color
						}}
					>
						Wochenübersicht
					</ListSubheader>
				}
			>
				{weekOverview.map((week, weekIdx) => (
					<div key={week}>
						<ListItem id={week} button onClick={handleItemClick}>
							<ListItemIcon sx={{ color: isDark ? themes.dark.color : themes.light.color }}>
								{isWeekOpen?.[week] ? <RemoveRedEyeIcon className={styles.navListIcon} fontSize='small' /> : <ExpandMoreIcon />}
							</ListItemIcon>
							<ListItemText primary={week} />
						</ListItem>
						{/* nested items */}
						<Collapse in={isWeekOpen?.[week]} timeout='auto' unmountOnExit>
							<List
								component='div'
								disablePadding
								onClick={toggleNav}
								onKeyDown={toggleNav}
							>
								{dayOverview.map((day, dayIdx) => (
									<Link key={`${week}-${day}`} href={`/woche/${weekIdx + 1}/tag/${dayIdx + 1}`}>
										<a>
											<ListItemButton
												sx={{
													pl: 5,
													backgroundColor: isActiveMenu(router, weekIdx, dayIdx) ? isDark ? themes.dark.activeMenu : themes.light.activeMenu : 'transparent'
												}}
											>
												<ListItemIcon sx={{ color: isDark ? themes.dark.color : themes.light.color }}>
													<ArrowRightIcon fontSize='small' />
												</ListItemIcon>
												<ListItemText primary={day} />
											</ListItemButton>
										</a>
									</Link>
								))}
							</List>
						</Collapse>
					</div>
				))}
			</List>
		</Box>
	)
}

const isActiveMenu = (router: NextRouter, weekIdx: number, dayIdx: number) => {
	if (!router.query.week || !router.query.day) {
		return false
	}
	return +router.query.week === weekIdx + 1 && +router.query.day === dayIdx + 1
}

const mainList = ['Home']

const weekOverview = [
	'Woche 1',
	'Woche 2',
	'Woche 3',
	'Woche 4',
	'Woche 5',
	'Woche 6',
	'Woche 7',
	'Woche 8',
	'Woche 9',
	'Woche 10',
	'Woche 11',
	'Woche 12',
	'Woche 13',
	'Woche 14',
	'Woche 15',
	'Woche 16',
	'Woche 17',
	'Woche 18',
	'Woche 19',
	'Woche 20',
	'Woche 21',
	'Woche 22'
]

const dayOverview = [
	'Tag 1',
	'Tag 2',
	'Tag 3',
	'Tag 4',
	'Tag 5'
]