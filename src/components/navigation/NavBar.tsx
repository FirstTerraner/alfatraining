import HomeIcon from '@mui/icons-material/Home'
import MenuIcon from '@mui/icons-material/Menu'
import { usePrefContext } from '../../context/preferences'
import { Box, IconButton } from '@mui/material'
import Link from 'next/link'
import { themes } from '../../../styles/colors'
import styles from '../../../styles/Nav.module.css'
import ThemeButton from '../ThemeButton'
import SettingsIcon from '@mui/icons-material/Settings'
import { useRouter } from 'next/router'

export default function NavBar({ WEEK, DAY, toggleNav }: { WEEK?: string, DAY?: string, toggleNav: () => void }) {
	const router = useRouter()
	const { isDark } = usePrefContext()
	return (
		<Box className={styles.NavBarWrap} sx={{ borderBottom: `1px solid ${isDark ? themes.dark.border : themes.light.border}` }}>
			<Box className='flex'>
				<IconButton onClick={toggleNav}>
					<MenuIcon sx={{ color: isDark ? themes.dark.color : themes.light.color }} />
				</IconButton>
				{WEEK && DAY && <h3 className={styles.navTxt}>Woche&nbsp;{WEEK} - Tag&nbsp;{DAY}</h3>}
			</Box>
			{WEEK && DAY ?
				<div className='flex'>
					<ThemeButton />
					<Link href='/'>
						<a>
							<div className={styles.homeBtn}>
								<HomeIcon />
							</div>
						</a>
					</Link>
				</div>
				:
				<div className='flex'>
					<Link href='/einstellungen'>
						<a className={styles.settingsBtn}>
							<SettingsIcon />
						</a>
					</Link>
					<ThemeButton />
					{router.asPath.includes('/einstellungen') &&
						<Link href='/'>
							<a>
								<div className={styles.homeBtn}>
									<HomeIcon />
								</div>
							</a>
						</Link>
					}
				</div>
			}
		</Box>
	)
}