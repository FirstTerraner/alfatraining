/* eslint-disable @next/next/no-css-tags */
import Head from 'next/head'
import { IMetatagsProps } from '../models'

export default function Metatags(props: IMetatagsProps) {
	const { title, description } = props
	return (
		<Head>
			<title>{title}</title>
			<meta name='description' content={description} />
			<meta name='viewport' content='initial-scale=1, width=device-width' />
			<link rel='icon' href='/favicon.ico' />
		</Head>
	)
}