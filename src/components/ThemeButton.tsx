import { usePrefContext } from '../context/preferences'
import styles from '../../styles/Nav.module.css'
import LightModeIcon from '@mui/icons-material/LightMode'
import DarkModeIcon from '@mui/icons-material/DarkMode'

export default function ThemeButton() {

	const { isDark, toggleDarkMode, cookieAllowed } = usePrefContext()

	if (!cookieAllowed) { return null }

	return (
		<div
			className={`${styles.homeBtn} pointer`}
			onClick={toggleDarkMode}
		>
			{isDark ? <LightModeIcon /> : <DarkModeIcon />}
		</div>
	)
}