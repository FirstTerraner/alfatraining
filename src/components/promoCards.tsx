import { Tooltip } from '@mui/material'
import { ICardProps } from '../models'
import styles from '../../styles/Home.module.css'
import { usePrefContext } from '../context/preferences'

export default function PromoCards({ cards }: ICardProps) {

	const { isDark } = usePrefContext()

	return (
		<div className={styles.grid}>
			{cards && cards.length > 0 ?
				cards.map(card => (
					<Tooltip key={card.header} title={card.tooltipTitle}>
						<a
							href={card.link}
							target='_blank'
							className={`${styles.card} ${isDark ? styles.darkBorder : styles.lightBorder}`}
							rel='noopener noreferrer'
						>
							<h2>{card.header}&nbsp;&rarr;</h2>
							<p>{card.description}</p>
						</a>
					</Tooltip>
				))
				:
				null
			}
		</div>
	)
}