import ArrowForwardIcon from '@mui/icons-material/ArrowForward'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { Stack, Button } from '@mui/material'
import Link from 'next/link'
import { isFirstDay, getPreviousDayPath, isLastDay, getNextDayPath } from '../helper'

export default function DayJumperButtons({ WEEK, DAY }: { WEEK?: string, DAY?: string }) {
	return (
		<Stack direction='row' spacing={2} sx={{ margin: '2em 0' }}>
			<Button
				variant='outlined'
				color='primary'
				disabled={isFirstDay(WEEK, DAY)}
				sx={{ color: isFirstDay(WEEK, DAY) ? '#666!important' : '' }}
			>
				<Link href={getPreviousDayPath(WEEK, DAY)}>
					<a className='flex'>
						<ArrowBackIcon fontSize='small' />&nbsp;Vorheriger Tag
					</a>
				</Link>
			</Button>
			<Button
				variant='outlined'
				color='primary'
				disabled={isLastDay(WEEK, DAY)}
				sx={{ color: isLastDay(WEEK, DAY) ? '#666!important' : '' }}
			>
				<Link href={getNextDayPath(WEEK, DAY)}>
					<a>
						Nächster Tag&nbsp;<ArrowForwardIcon fontSize='small' />
					</a>
				</Link>
			</Button>
		</Stack>
	)
}