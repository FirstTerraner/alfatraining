import Link from 'next/link'
import Image from 'next/image'
import styles from '../../styles/Home.module.css'

interface IImageSectionProps {
	imgSrc: string
	width: number
	height: number
	imgReference: string
	referenceTxt: string[]
}

export default function ImageSection({ imgSrc, width, height, imgReference, referenceTxt }: IImageSectionProps) {
	return (
		<div style={{ margin: 'auto' }}>
			<Image
				src={imgSrc}
				alt=''
				width={width}
				height={height}
				placeholder='empty'
				quality={100}
				loading='lazy'
			/>
			<p>
				{referenceTxt[0]}&nbsp;
				{referenceTxt[1] &&
					<Link href={imgReference}>
						<a target='_blank' rel='noopener noreferrer' className={styles.highlightedLink} >
							{referenceTxt[1]}
						</a>
					</Link>
				}
			</p>
		</div>
	)
}