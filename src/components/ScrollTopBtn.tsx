import { IconButton } from '@mui/material'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import React from 'react'
import { getScrollPos } from '../helper'
import Navigation from './navigation/Navigation'
import { usePrefContext } from '../context/preferences'

export default function ScrollTopButton({ WEEK, DAY }: { WEEK?: string, DAY?: string }) {

	const [isDownScrolled, setIsDownScrolled] = React.useState(false)

	const { isDark } = usePrefContext()

	React.useEffect(() => {
		// add scroll listener
		window.onscroll = () => {
			if (getScrollPos() > 200) {
				setIsDownScrolled(true)
				return
			}
			setIsDownScrolled(false)
		}
	}, [])

	const handleClick = () => {
		window.scroll(0, 0)
	}

	return isDownScrolled ?
		<>
			<div className={`scrolledDownNavWrap gonimate fadeInDown ${isDark ? 'darkTheme' : 'lightTheme'}`}>
				<Navigation WEEK={WEEK} DAY={DAY} />
			</div>
			<div className='ScrollBtnWrap'>
				<IconButton
					size='large'
					sx={{ backgroundColor: '#1976d2', color: '#c1c1c1' }}
					onClick={handleClick}
				>
					<ArrowUpwardIcon />
				</IconButton>
			</div>
		</>

		:
		null
}