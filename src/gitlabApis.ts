import axios from 'axios'
import { IGitProject, IProjectMember } from './models'

const PROJECT_ID = '33094829'

const baseReq = axios.create({ baseURL: 'https://gitlab.com/api/v4', timeout: 5000, headers: { 'PRIVATE-TOKEN': process.env.PRIVATE_TOKEN || '' } })

const fetchRoute = async <T>(route: string) => {
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		// const cached: T = cache.get(route)
		// console.log('frontend cached: ', cached)
		// if (cached) {
		// 	return cached
		// }
		const resp = await baseReq.get<T>(route)
		// console.log('response in cacheFetch: ', resp)
		// bad status code or null resp
		if ((resp?.status && (resp?.status < 200 || resp?.status > 299)) || !resp?.data) { return null }
		// add to cache
		// cache.put(route, resp.data, 1000 * 60 * 3) // 3mins
		return resp.data
	} catch (e) { console.error('url: ', route, 'error: ', e) }
	return null
}
// for more gitlab endpoints visit: https://docs.gitlab.com/ee/api/api_resources.html

// https://docs.gitlab.com/ee/api/projects.html#get-single-project
export const getGitlabProject = async () => {
	const resp = await fetchRoute<IGitProject>(`/projects/${PROJECT_ID}`)
	if (!resp) { return null }
	return resp
}

// https://docs.gitlab.com/ee/api/members.html#list-all-members-of-a-group-or-project
export const getAllProjectMembers = async () => {
	const resp = await fetchRoute<IProjectMember[]>(`/projects/${PROJECT_ID}/members`)
	if (!resp) { return null }
	return resp
}

// open_issues_count
// created_at
// last_activity_at
// avatar_url
// statistics.
// _links.issues
// forks_count
// star_count

// "marked_for_deletion_at": "2020-04-03", // Deprecated and will be removed in API v5 in favor of marked_for_deletion_on

// "container_expiration_policy" . "name_regex": null, // to be deprecated in GitLab 13.0 in favor of `name_regex_delete`

// "container_registry_enabled": false, // deprecated, use container_registry_access_level instead

// "tag_list": [ //deprecated, use `topics` instead