import { toJsonObject, toJsonString } from '../../helper'
import { IPreferences } from '../../models'
import { setCookie, getCookie } from './handler'

export const setPrefCookie = (pref: IPreferences, days: number) => {
	if (!pref) { return }
	const prefStr = toJsonString(pref)
	if (prefStr) { setCookie('Prefs', prefStr, days) }
}

export const getPrefCookie = () => {
	const prefs = getCookie('Prefs')
	if (!prefs) { return null }
	const prefsObj = toJsonObject<IPreferences>(prefs)
	if (!prefsObj) { return null }
	return prefsObj
}