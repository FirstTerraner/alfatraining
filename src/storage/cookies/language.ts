import { TLocale } from '../../models'
import { getCookie, setCookie } from './handler'

export const setLanguageCookie = (val: TLocale, days: number) => setCookie('NEXT_LOCALE', val, days)

export const getLanguageCookie = () => getCookie('NEXT_LOCALE')