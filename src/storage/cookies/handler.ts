export const setCookie = (key: string, value: string, days: number) => {
	let expires = ''
	if (days) {
		const date = new Date()
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000))
		expires = `; expires=${date.toUTCString()}`
	}
	document.cookie = `${key}=${value}${expires}; SameSite=Lax; Secure; path=/`
}

export const getCookie = (key: string) => {
	const nameEQ = key + '='
	const splittedCookie = document.cookie.split(';')
	for (let i = 0; i < splittedCookie.length; i++) {
		let crumb = splittedCookie[i]
		while (crumb.charAt(0) === ' ') {
			crumb = crumb.substring(1, crumb.length)
		}
		if (crumb.indexOf(nameEQ) === 0) {
			return crumb.substring(nameEQ.length, crumb.length)
		}
	}
	return null
}