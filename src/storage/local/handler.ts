import { toJsonObject } from '../../helper'

export function getFromLocalStorage<T>(key: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try {
		const resp = localStorage.getItem(prefix ? `${prefix}||${key}` : key)
		if (!resp) { return null }
		const jsonResp: T | null = toJsonObject<T>(resp)
		return jsonResp ? jsonResp : null
	} catch (e) { console.log(e) }
	return null
}

export const setLocalStorage = (key: string, val: string, prefix?: string) => {
	if (!key || !localStorage) { return null }
	try { return localStorage.setItem(prefix ? `${prefix}||${key}` : key, val) } catch (e) { console.log(e) }
	return null
}

export const removeFromLocalStorage = (key: string, prefix?: string) => {
	if (!key || !localStorage) { return null }
	try { return localStorage.removeItem(prefix ? `${prefix}||${key}` : key) } catch (e) { console.log(e) }
	return null
}

export const getAllKeysByPrefixFromLocalStorage = (prefix: string) =>
	getAllKeysFromLocalStorage()?.filter(x => x?.trim() && x?.startsWith(prefix))

export const getAllKeysFromLocalStorage = () => {
	if (!localStorage) { return null }
	const keys = []
	for (let i = 0; i < localStorage.length; i++) { keys.push(localStorage.key(i)) }
	return keys.filter(x => x?.trim())
}