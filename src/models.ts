export type TWeekArrayState = {
	[key: string]: boolean
}

export interface IMetatagsProps {
	title: string
	description: string
}
export interface IHomeProps {
	members: IProjectMember[] | null
	gitProject: IGitProject | null
}
export interface IProjectMember {
	id: number
	username: string
	name: string
	state: string
	avatar_url: string
	web_url: string
	expires_at: string
	access_level: TMemberAccessLvl
	group_saml_identity: ISAMLID | null
}

interface ISAMLID {
	extern_uid: string
	provider: string
	saml_provider_id: number
}

type TMemberAccessLvl = 0 | 5 | 10 | 20 | 30 | 40

export type TLocale = 'en-US' | 'de-DE'

export const isTLocale = (lType: unknown) => lType === 'de-DE' || lType === 'fr-FR' || lType === 'en-US' || !lType

export interface ICardProps {
	cards: ICard[]
}
export interface ICard {
	header: string
	description: string
	link: string
	tooltipTitle: string
}

type TLinkProps = {
	url: string
	description: string
}
export interface ITopicProps {
	header?: string
	description?: string
	list?: string[]
	linkList?: TLinkProps[]
	html?: string
	css?: string
	code?: string
	codeLanguage?: string
	refLinks?: TLinkProps[]
}

export interface IGitProject {
	id: number
	description: string
	name: string
	name_with_namespace: string
	path: string
	path_with_namespace: string
	created_at: string
	default_branch: string
	tag_list: any[]
	topics: any[]
	ssh_url_to_repo: string
	http_url_to_repo: string
	web_url: string
	readme_url: string
	avatar_url: string
	forks_count: number
	star_count: number
	last_activity_at: string
	namespace: Namespace
	container_registry_image_prefix: string
	_links: Links
	packages_enabled: boolean
	empty_repo: boolean
	archived: boolean
	visibility: string
	owner: Owner
	resolve_outdated_diff_discussions: boolean
	container_expiration_policy: ContainerExpirationPolicy
	issues_enabled: boolean
	merge_requests_enabled: boolean
	wiki_enabled: boolean
	jobs_enabled: boolean
	snippets_enabled: boolean
	container_registry_enabled: boolean
	service_desk_enabled: boolean
	service_desk_address: string
	can_create_merge_request_in: boolean
	issues_access_level: string
	repository_access_level: string
	merge_requests_access_level: string
	forking_access_level: string
	wiki_access_level: string
	builds_access_level: string
	snippets_access_level: string
	pages_access_level: string
	operations_access_level: string
	analytics_access_level: string
	container_registry_access_level: string
	emails_disabled: null
	shared_runners_enabled: boolean
	lfs_enabled: boolean
	creator_id: number
	import_status: string
	import_error: null
	open_issues_count: number
	runners_token: string
	ci_default_git_depth: number
	ci_forward_deployment_enabled: boolean
	ci_job_token_scope_enabled: boolean
	public_jobs: boolean
	build_git_strategy: string
	build_timeout: number
	auto_cancel_pending_pipelines: string
	build_coverage_regex: null
	ci_config_path: string
	shared_with_groups: any[]
	only_allow_merge_if_pipeline_succeeds: boolean
	allow_merge_on_skipped_pipeline: null
	restrict_user_defined_variables: boolean
	request_access_enabled: boolean
	only_allow_merge_if_all_discussions_are_resolved: boolean
	remove_source_branch_after_merge: boolean
	printing_merge_request_link_enabled: boolean
	merge_method: string
	squash_option: string
	suggestion_commit_message: null
	merge_commit_template: null
	squash_commit_template: null
	auto_devops_enabled: boolean
	auto_devops_deploy_strategy: string
	autoclose_referenced_issues: boolean
	keep_latest_artifact: boolean
	runner_token_expiration_interval: null
	approvals_before_merge: number
	mirror: boolean
	external_authorization_classification_label: string
	marked_for_deletion_at: null
	marked_for_deletion_on: null
	requirements_enabled: boolean
	security_and_compliance_enabled: boolean
	compliance_frameworks: any[]
	issues_template: null
	merge_requests_template: null
	merge_pipelines_enabled: boolean
	merge_trains_enabled: boolean
	permissions: Permissions
}

export interface Links {
	self: string
	issues: string
	merge_requests: string
	repo_branches: string
	labels: string
	events: string
	members: string
}

export interface ContainerExpirationPolicy {
	cadence: string
	enabled: boolean
	keep_n: number
	older_than: string
	name_regex: string
	name_regex_keep: null
	next_run_at: string
}

export interface Namespace {
	id: number
	name: string
	path: string
	kind: string
	full_path: string
	parent_id: null
	avatar_url: string
	web_url: string
}

export interface Owner {
	id: number
	username: string
	name: string
	state: string
	avatar_url: string
	web_url: string
}

export interface Permissions {
	project_access: ProjectAccess
	group_access: null
}

export interface ProjectAccess {
	access_level: number
	notification_level: number
}

export interface IPreferences {
	dark: boolean
	cookieAllowed: boolean
}