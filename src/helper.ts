import { NextRouter } from 'next/router'

export const isLastDay = (WEEK?: string, DAY?: string) => {
	if (!WEEK || !DAY) { return false }
	return WEEK === '22' && DAY === '5'
}

export const isFirstDay = (WEEK?: string, DAY?: string) => {
	if (!WEEK || !DAY) { return false }
	return WEEK === '1' && DAY === '1'
}

export const getNextDayPath = (WEEK?: string, DAY?: string) => {
	// dont do anything
	if (WEEK && DAY && isLastDay(WEEK, DAY)) { return '#' }
	// increment week, reset day
	if (WEEK && DAY && +WEEK < 22 && DAY === '5') { return `/woche/${+WEEK + 1}/tag/1` }
	// increment day only
	if (WEEK && DAY && +WEEK <= 22 && +DAY < 5) { return `/woche/${WEEK}/tag/${+DAY + 1}` }
	// else
	return '#'
}

export const getPreviousDayPath = (WEEK?: string, DAY?: string) => {
	// dont do anything
	if (WEEK && DAY && isFirstDay(WEEK, DAY)) { return '#' }
	// decrement week, reset day
	if (WEEK && DAY && +WEEK <= 22 && DAY === '1') { return `/woche/${+WEEK - 1}/tag/5` }
	// decrement day only
	if (WEEK && DAY && +WEEK >= 1 && +DAY > 1) { return `/woche/${WEEK}/tag/${+DAY - 1}` }
	// else
	return '#'
}

export const isBadPath = (WEEK?: string, DAY?: string) => {
	if (WEEK && (+WEEK > 22 || +WEEK < 1 || !+WEEK)) { return true }
	if (DAY && (+DAY > 5 || +DAY < 1 || !+DAY)) { return true }
	if (!WEEK || !DAY) { return true }
	return false
}

export const handleTitleClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>, router: NextRouter) => {
	const id = e.currentTarget.id
	if (!id) { return }
	void router.push(`#${id}`)
}

// wrap JSON Methods in try catch
export function toJsonObject<T>(json?: string | null) {
	if (!json || json?.length < 2) { return null }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const thing: T = JSON.parse(json)
		return thing
	} catch (e) { console.error(e) }
	return null
}

// wrap JSON Methods in try catch
export function toJsonString<T>(thing: T) {
	try {
		const jsonStr = JSON.stringify(thing)
		return jsonStr
	} catch (e) {
		return null
	}
}

export function getScrollPos() {
	return window.pageYOffset || document.documentElement.scrollTop
}